import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
sns.set(style="whitegrid")
s = np.random.normal(10,5,1000)
ax = sns.violinplot(y=s)
plt.show()

y = np.random.lognormal(10,5,1000)
ay = sns.violinplot(y=y)

plt.show()

