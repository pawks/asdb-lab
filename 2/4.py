import numpy as np
np.random.seed(19680801)
import matplotlib.pyplot as plt
from scipy.stats import pearsonr


fig, ax = plt.subplots()
for color in ['tab:blue']:
    n = 750
    x=[0.45,0.91,1.36,1.81,2.27]
    y=[3.6,6.7,9.8,11.2,14.7]
    scale = 100
    ax.scatter(x, y, c=color, s=scale, label=color,
               alpha=0.3, edgecolors='none')

    corr, _ = pearsonr(x,y)
    print(corr)

ax.legend()
ax.grid(True)

plt.show()


