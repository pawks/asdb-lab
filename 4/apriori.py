import csv
from apyori import apriori
records = []
with open('./datasets/store.dat',"r") as csvfile:
    csv_reader = csv.reader(csvfile,delimiter = " ")
    for entry in csv_reader:
        records.append([entry[j] for j in range(len(entry))])
association_rules = apriori(records, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2)
association_rules = list(association_rules)
print(len(association_rules))
for item in association_rules:
    pair = item[0]
    items = [x for x in pair]
    print("Rule: " + items[0] + " -> " + items[1])
    print("Support: " + str(item[1]))
    print("Confidence: " + str(item[2][0][2]))
    print("Lift: " + str(item[2][0][3]))
    print("=====================================")
