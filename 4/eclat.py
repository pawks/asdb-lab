import csv
from fim import eclat
records = []
with open('./datasets/connect.dat',"r") as csvfile:
    csv_reader = csv.reader(csvfile,delimiter = " ")
    for entry in csv_reader:
        records.append([entry[j] for j in range(len(entry))])
itemsets = eclat(records,supp = 90, zmax = 10)
print(itemsets)
