from fp_growth import find_frequent_itemsets
import pyfpgrowth
import csv
records = []
with open('./datasets/store.dat',"r") as csvfile:
    csv_reader = csv.reader(csvfile,delimiter = " ")
    for entry in csv_reader:
        records.append([entry[j] for j in range(len(entry))])
patterns = find_frequent_itemsets(records, 2)
rules = pyfpgrowth.generate_association_rules(patterns, 0.7)
print(len(rules))
print(rules)
#for item in association_rules:
#    pair = item[0]
#    items = [x for x in pair]
#    print("Rule: " + items[0] + " -> " + items[1])
#    print("Support: " + str(item[1]))
#    print("Confidence: " + str(item[2][0][2]))
#    print("Lift: " + str(item[2][0][3]))
#    print("=====================================")
#
