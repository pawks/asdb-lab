from sklearn.preprocessing import StandardScaler
import numpy as np
data = np.array([13, 15, 16, 16, 19, 20, 20, 21, 22, 22, 25, 25, 25, 25, 30, 33,
33, 35, 35, 35, 35, 36, 40, 45, 46, 52, 70])
data = data.reshape(-1,1)
scaler = StandardScaler()
print(scaler.fit(data))
print(scaler.transform(data))
print(scaler.transform((np.array([25])).reshape(-1,1)))


