import numpy as np
def Dec_scale(df):
    p = df.max()
    q = len(str(abs(p)))
    df = df/10**q
    return df

data = np.array([13, 15, 16, 16, 19, 20, 20, 21, 22, 22, 25, 25, 25, 25, 30, 33,
33, 35, 35, 35, 35, 36, 40, 45, 46, 52, 70])

print(Dec_scale(data))
