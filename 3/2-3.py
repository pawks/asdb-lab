import pandas as pd
import matplotlib.pyplot as plt
import numpy
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
df.loc[df['year'] > 2016, 'year'] = 1.0
df.loc[df['year'] <= 2016, 'year'] = 0.0
print(df)
