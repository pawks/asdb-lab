import pandas as pd
import matplotlib.pyplot as plt
import numpy
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
hs1 = df.hist(column='Total Volume',bins=50)
plt.show()
hs2 = df.hist(column='Total Volume',bins=250)
plt.show()
data = numpy.sort(numpy.array(df.loc[:,'Total Volume']))
statistics = stats.binned_statistic(data,data,bins=50)
df = pd.DataFrame(statistics[0])
print(df)
hs3 = df.hist(bins=50)
plt.show()
df = pd.DataFrame(statistics[1])
print(df)
hs4 = df.hist(bins=50)
plt.show()
statistics = stats.binned_statistic(data,data,'median',bins=50)
df = pd.DataFrame(statistics[0])
print(df)
