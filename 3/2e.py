import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
df = df.astype({'year':'object'})
#print(df)
df.replace({'year': {2015: 'Old', 2016: 'Old', 2017: 'New', 2018: 'Recent'}},inplace=True)
print(df)
