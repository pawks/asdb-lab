import pandas as pd
import matplotlib.pyplot as plt
import numpy
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
df = df.drop('AveragePrice',1)
print(df.describe())
print("SKEW")
print(df.skew(axis=0))
print("CORR")
print(df.corr())
if (df['type']=='organic').count() == (df['type']=='conventional').count():
    print("Balanced")
else:
    print("Imbalanced")
