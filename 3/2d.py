import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
df = df.astype({'AveragePrice':'float64'})
null_data = df[df.isnull().any(axis=1)]
df['AveragePrice'].fillna(df.groupby('region')['AveragePrice'].transform('mean'),inplace=True)
print(df.loc[5:])

