import pandas as pd
import matplotlib.pyplot as plt
import numpy
from scipy import stats
csv_file = 'avocado.csv'
df = pd.read_csv(csv_file)
data=numpy.array(df.loc[:,["Date","Total Volume"]])
month = {}
year = {}
for entry in data:
    date = entry[0].split('/')
    try:
        month[date[0]+'/'+date[2]]+=entry[1]
    except KeyError:
        month[date[0]+'/'+date[2]]=entry[1]
    try:
        year[date[2]]+=entry[1]
    except KeyError:
        year[date[2]]=entry[1]
print(month)
print()
print(year)
