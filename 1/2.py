import statistics
rstr='CSE20D{:02d}'
roll_num = []
marks = []
for i in range(1,19):
    roll_num.append(rstr.format(i))
    if i%2==0:
        marks.append(25+((i+8)%10))
    else:
        marks.append(25+((i+7)%10))
print(list(zip(roll_num,marks)))
print("Mean",statistics.mean(marks))
print("Median",statistics.median(marks))
