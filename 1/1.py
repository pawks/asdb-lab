import csv
count={
    'Red':0,'Black':0,'Blue':0,'Green':0,'Yellow':0
}
with open("1.csv","r") as file:
    data = csv.reader(file, delimiter=',')
    for row in data:
        count[row[1]]+=1
for entry in count:
    output=entry+" "
    for i in range(1,count[entry]+1):
        if i%5==0:
            output+="/ "
        else:
            output+="|"
    output+=" "+str(count[entry])
    print(output)
