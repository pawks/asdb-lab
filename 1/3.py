from random import randint
from statistics import stdev
x = []
y = []
for i in range(20):
    tx = randint(5,1000)
    x.append(tx)
    y.append(2*tx+3)
print(list(zip(x,y)))
print("STD:",stdev(y))
