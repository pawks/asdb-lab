import matplotlib.pyplot as plt

# Data to plot
labels = ['S', 'B', 'C', 'D']
sizes =[31,29,25,15]
colors = ['gold', 'yellowgreen', 'lightcoral', 'lightskyblue']
explode = (0, 0, 0, 0)  # explode 1st slice

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,autopct='%1.1f%%', shadow=False, startangle=140)

plt.axis('equal')
plt.show()

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(labels,sizes)
plt.show()
