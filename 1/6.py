import matplotlib.pyplot as plt

# Data to plot
labels = ['Studying', 'Sleeping', 'Playing', 'Hobbies',"Others"]
sizes =[33,30,18,5,14]
colors = ['gold', 'blue', 'yellowgreen', 'lightcoral', 'lightskyblue']
explode = (0, 0, 0, 0, 0)  # explode 1st slice

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,autopct='%1.1f%%', shadow=False, startangle=140)

plt.axis('equal')
plt.show()
