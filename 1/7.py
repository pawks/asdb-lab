import statistics
import argparse
import pathlib
import csv
class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        raise SystemExit

def cmdline_args():
    parser = MyParser(
        prog="grader",
        description="This program checks compliance for a DUT.")
    parser.add_argument('--absolute',
                        action='store_true',
                        help='Absolute Grading.')
    parser.add_argument('--relative',
                        action='store_true',
                        help='Relative Grading.')
    parser.add_argument('--input',
                        type= lambda p: str(pathlib.Path(p).absolute()),
                        action='store',
                        help='The Path to the input file.',
                        required=True,
                        metavar= 'PATH')
    return parser

def main():
    parser = cmdline_args()
    args = parser.parse_args()
    grades = {
        'S':0, 'A':0, 'B':0, 'C':0, 'D':0, 'E':0, 'F':0
    }
    total=[]
    with open(args.input,"r") as file:
        data = list(csv.reader(file, delimiter=','))
        for i in range(1,len(data)):
            data[i].append(int(data[i][1])+int(data[i][2])+int(data[i][3]))
            total.append(data[i][4])
        if args.absolute:
            e_cutoff = 0.5 * statistics.mean(total)
            for i in range(1,len(data)):
                grade = ''
                if data[i][4]>=90:
                    grade = 'S'
                elif data[i][4]>=80:
                    grade = 'A'
                elif data[i][4]>=70:
                    grade = 'B'
                elif data[i][4]>=60:
                    grade = 'C'
                elif data[i][4]>=50:
                    grade = 'D'
                elif data[i][4]>=e_cutoff:
                    grade = 'E'
                else:
                    grade = 'F'
                data[i].append(grade)
                grades[grade]+=1
                print(data[i])
            print(grades)
        if args.relative:
            avg = statistics.mean(total)
            pass_min = avg/2
            pass_mean = statistics.mean([val for val in total if val>pass_min])
            x = pass_mean - pass_min
            max_mark = max(total)
            s_cutoff = max_mark - 0.1*(max_mark-pass_mean)
            y = s_cutoff-pass_mean
            a_cutoff = pass_mean+y*5/8
            b_cutoff = pass_mean+y*2/8
            c_cutoff = pass_mean-x*2/8
            d_cutoff = pass_mean-x*5/8
            e_cutoff = pass_min
            for i in range(1,len(data)):
                grade = ''
                if data[i][4]>=s_cutoff:
                    grade = 'S'
                elif data[i][4]>=a_cutoff:
                    grade = 'A'
                elif data[i][4]>=b_cutoff:
                    grade = 'B'
                elif data[i][4]>=c_cutoff:
                    grade = 'C'
                elif data[i][4]>=d_cutoff:
                    grade = 'D'
                elif data[i][4]>=e_cutoff:
                    grade = 'E'
                else:
                    grade = 'F'
                data[i].append(grade)
                grades[grade]+=1
                print(data[i])
            print(grades)


if __name__=='__main__':
    main()
