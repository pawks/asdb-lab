import statistics as st
import pandas as pd
height=[167.65, 167, 172, 175, 165,167, 168, 167, 167.3, 170, 167.5, 170, 167, 169,172]
print("Mean",st.mean(height))
print("Median",st.median(height))
print("Mode",st.mode(height))
print("STDev",st.stdev(height))
print("Skew",(pd.DataFrame(height)).skew())
