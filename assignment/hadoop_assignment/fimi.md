```python
! wget http://fimi.uantwerpen.be/data/retail.dat.gz
```

    --2020-05-09 18:54:09--  http://fimi.uantwerpen.be/data/retail.dat.gz
    Resolving fimi.uantwerpen.be (fimi.uantwerpen.be)... 143.129.69.1
    Connecting to fimi.uantwerpen.be (fimi.uantwerpen.be)|143.129.69.1|:80... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 1636819 (1.6M) [application/x-gzip]
    Saving to: ‘retail.dat.gz’
    
    retail.dat.gz       100%[===================>]   1.56M  1.35MB/s    in 1.2s    
    
    2020-05-09 18:54:11 (1.35 MB/s) - ‘retail.dat.gz’ saved [1636819/1636819]
    



```python
! gunzip retail.dat.gz
```


```python
from pyspark import SparkContext
sc = SparkContext(appName="PythonCorrelations")
```


```python
from pyspark.mllib.fpm import FPGrowth

data = sc.textFile("retail.dat")

transactions = data.map(lambda line: line.strip().split(' '))

model = FPGrowth.train(transactions, minSupport=0.1, numPartitions=100)

result = model.freqItemsets().collect()
for fi in result:
    print(fi)
```

    FreqItemset(items=['39'], freq=50675)
    FreqItemset(items=['48'], freq=42135)
    FreqItemset(items=['48', '39'], freq=29142)
    FreqItemset(items=['38'], freq=15596)
    FreqItemset(items=['38', '39'], freq=10345)
    FreqItemset(items=['32'], freq=15167)
    FreqItemset(items=['41'], freq=14945)
    FreqItemset(items=['41', '48'], freq=9018)
    FreqItemset(items=['41', '39'], freq=11414)

