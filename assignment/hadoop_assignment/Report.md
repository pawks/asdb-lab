---
title: "Hadoop Assignment"
author: [Group - 11, Ajay Narayanan - COE16B044, Chinmay Mahajan - CED16I023, Shruti Raghavan - CED16I030, Shukrithi Rathna - CED16I032, S Pawan Kumar - CED16I043]
date: "2020-05-21"
titlepage: true
table-use-row-colors: true
classoption: [oneside]
footer-left: "Group-11"
...

# Mandatory Questions
## 1
### a. Word Count
* Framework used - Hadoop
* input file -
> ```
> one
> two
> two
> three
> three
> three
> ```

* output file -
> ```
> one	1
> three	3
> two	2
> ```

* Log
    
> ``` shell
> 
> bitnami@hadoop:~$ cd code/wordcount/
> bitnami@hadoop:~/code/wordcount$ ls
> sample.txt  WordCount.class                WordCount.java
> wc.jar      WordCount$IntSumReducer.class  WordCount$TokenizerMapper.class
> bitnami@hadoop:~$ hadoop jar ./wordcount/wc.jar WordCount /wordcount/input /wordcount/output
> 2020-05-09 05:52:51,202 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
> 2020-05-09 05:52:51,786 WARN mapreduce.JobResourceUploader: Hadoop command-line option parsing not performed. Implement the Tool interface and execute your application with ToolRunner to remedy this.
> 2020-05-09 05:52:51,810 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589001412574_0002
> 2020-05-09 05:52:51,968 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
> 2020-05-09 05:52:52,135 INFO input.FileInputFormat: Total input files to process : 1
> 2020-05-09 05:52:52,178 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
> 2020-05-09 05:52:52,206 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
> 2020-05-09 05:52:52,215 INFO mapreduce.JobSubmitter: number of splits:1
> 2020-05-09 05:52:52,394 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
> 2020-05-09 05:52:52,438 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589001412574_0002
> 2020-05-09 05:52:52,439 INFO mapreduce.JobSubmitter: Executing with tokens: []
> 2020-05-09 05:52:52,678 INFO conf.Configuration: resource-types.xml not found
> 2020-05-09 05:52:52,679 INFO resource.ResourceUtils: Unable to find 'resource-types.xml'.
> 2020-05-09 05:52:52,753 INFO impl.YarnClientImpl: Submitted application application_1589001412574_0002
> 2020-05-09 05:52:52,798 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589001412574_0002/
> 2020-05-09 05:52:52,799 INFO mapreduce.Job: Running job: job_1589001412574_0002
> 2020-05-09 05:53:01,030 INFO mapreduce.Job: Job job_1589001412574_0002 running in uber mode : false
> 2020-05-09 05:53:01,032 INFO mapreduce.Job:  map 0% reduce 0%
> 2020-05-09 05:53:07,114 INFO mapreduce.Job:  map 100% reduce 0%
> 2020-05-09 05:53:12,165 INFO mapreduce.Job:  map 100% reduce 100%
> 2020-05-09 05:53:13,184 INFO mapreduce.Job: Job job_1589001412574_0002 completed successfully
> 2020-05-09 05:53:13,314 INFO mapreduce.Job: Counters: 54
> 	File System Counters
> 		FILE: Number of bytes read=38
> 		FILE: Number of bytes written=451493
> 		FILE: Number of read operations=0
> 		FILE: Number of large read operations=0
> 		FILE: Number of write operations=0
> 		HDFS: Number of bytes read=143
> 		HDFS: Number of bytes written=20
> 		HDFS: Number of read operations=8
> 		HDFS: Number of large read operations=0
> 		HDFS: Number of write operations=2
> 		HDFS: Number of bytes read erasure-coded=0
> 	Job Counters 
> 		Launched map tasks=1
> 		Launched reduce tasks=1
> 		Data-local map tasks=1
> 		Total time spent by all maps in occupied slots (ms)=6780
> 		Total time spent by all reduces in occupied slots (ms)=6748
> 		Total time spent by all map tasks (ms)=3390
> 		Total time spent by all reduce tasks (ms)=3374
> 		Total vcore-milliseconds taken by all map tasks=3390
> 		Total vcore-milliseconds taken by all reduce tasks=3374
> 		Total megabyte-milliseconds taken by all map tasks=6942720
> 		Total megabyte-milliseconds taken by all reduce tasks=6909952
> 	Map-Reduce Framework
> 		Map input records=6
> 		Map output records=6
> 		Map output bytes=54
> 		Map output materialized bytes=38
> 		Input split bytes=113
> 		Combine input records=6
> 		Combine output records=3
> 		Reduce input groups=3
> 		Reduce shuffle bytes=38
> 		Reduce input records=3
> 		Reduce output records=3
> 		Spilled Records=6
> 		Shuffled Maps =1
> 		Failed Shuffles=0
> 		Merged Map outputs=1
> 		GC time elapsed (ms)=255
> 		CPU time spent (ms)=1560
> 		Physical memory (bytes) snapshot=671588352
> 		Virtual memory (bytes) snapshot=6912757760
> 		Total committed heap usage (bytes)=598736896
> 		Peak Map Physical memory (bytes)=444260352
> 		Peak Map Virtual memory (bytes)=3451957248
> 		Peak Reduce Physical memory (bytes)=227328000
> 		Peak Reduce Virtual memory (bytes)=3460800512
> 	Shuffle Errors
> 		BAD_ID=0
> 		CONNECTION=0
> 		IO_ERROR=0
> 		WRONG_LENGTH=0
> 		WRONG_MAP=0
> 		WRONG_REDUCE=0
> 	File Input Format Counters 
> 		Bytes Read=30
> 	File Output Format Counters 
> 		Bytes Written=20
> bitnami@hadoop:~/code$ hadoop fs -cat /wordcount/output/part*
> 2020-05-09 05:55:28,565 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
> one	1
> three	3
> two	2
> 
> ```

### b. Matrix Multiplication

* Framework used - Hadoop
* input files
    * M

    > ```
    > M,1,1,-2.0
    > M,0,0,5.0
    > M,2,2,6.0
    > M,0,1,-3.0
    > M,3,2,7.0
    > M,0,2,-1.0
    > M,1,0,3.0
    > M,1,2,4.0
    > M,2,0,1.0
    > M,3,0,-4.0
    > M,3,1,2.0
    > ```
    * N

    > ```
    > N,1,0,3.0
    > N,0,0,5.0
    > N,1,2,-2.0
    > N,2,0,9.0
    > N,0,1,-3.0
    > N,0,2,-1.0
    > N,1,1,8.0
    > N,2,1,4.0
    > ```

* Output

> ```
> 0,0,7.0
> 0,1,-43.0
> 0,2,1.0
> 1,0,45.0
> 1,1,-9.0
> 1,2,1.0
> 2,0,59.0
> 2,1,21.0
> 2,2,-1.0
> 3,0,49.0
> 3,1,56.0
> ```

* Log

> ``` shell
>   bitnami@hadoop:~/code$ hadoop jar ./mmul/mmul.jar MatrixMultiply /matrix/input /matrix/output
>   2020-05-09 08:31:59,576 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>   2020-05-09 08:32:00,066 WARN mapreduce.JobResourceUploader: Hadoop command-line option parsing not performed. Implement the Tool interface and execute your application with ToolRunner to remedy this.
>   2020-05-09 08:32:00,085 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589001412574_0023
>   2020-05-09 08:32:00,254 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>   2020-05-09 08:32:00,462 INFO input.FileInputFormat: Total input files to process : 2
>   2020-05-09 08:32:00,499 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>   2020-05-09 08:32:00,531 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>   2020-05-09 08:32:00,541 INFO mapreduce.JobSubmitter: number of splits:2
>   2020-05-09 08:32:00,729 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>   2020-05-09 08:32:00,763 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589001412574_0023
>   2020-05-09 08:32:00,763 INFO mapreduce.JobSubmitter: Executing with tokens: []
>   2020-05-09 08:32:01,013 INFO conf.Configuration: resource-types.xml not found
>   2020-05-09 08:32:01,013 INFO resource.ResourceUtils: Unable to find 'resource-types.xml'.
>   2020-05-09 08:32:01,086 INFO impl.YarnClientImpl: Submitted application application_1589001412574_0023
>   2020-05-09 08:32:01,134 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589001412574_0023/
>   2020-05-09 08:32:01,134 INFO mapreduce.Job: Running job: job_1589001412574_0023
>   2020-05-09 08:32:08,343 INFO mapreduce.Job: Job job_1589001412574_0023 running in uber mode : false
>   2020-05-09 08:32:08,344 INFO mapreduce.Job:  map 0% reduce 0%
>   2020-05-09 08:32:17,463 INFO mapreduce.Job:  map 100% reduce 0%
>   2020-05-09 08:32:23,509 INFO mapreduce.Job:  map 100% reduce 100%
>   2020-05-09 08:32:24,526 INFO mapreduce.Job: Job job_1589001412574_0023 completed successfully
>   2020-05-09 08:32:24,667 INFO mapreduce.Job: Counters: 54
>   	File System Counters
>   		FILE: Number of bytes read=308916
>   		FILE: Number of bytes written=1296791
>   		FILE: Number of read operations=0
>   		FILE: Number of large read operations=0
>   		FILE: Number of write operations=0
>   		HDFS: Number of bytes read=399
>   		HDFS: Number of bytes written=97
>   		HDFS: Number of read operations=11
>   		HDFS: Number of large read operations=0
>   		HDFS: Number of write operations=2
>   		HDFS: Number of bytes read erasure-coded=0
>   	Job Counters 
>   		Launched map tasks=2
>   		Launched reduce tasks=1
>   		Data-local map tasks=2
>   		Total time spent by all maps in occupied slots (ms)=26806
>   		Total time spent by all reduces in occupied slots (ms)=8120
>   		Total time spent by all map tasks (ms)=13403
>   		Total time spent by all reduce tasks (ms)=4060
>   		Total vcore-milliseconds taken by all map tasks=13403
>   		Total vcore-milliseconds taken by all reduce tasks=4060
>   		Total megabyte-milliseconds taken by all map tasks=27449344
>   		Total megabyte-milliseconds taken by all reduce tasks=8314880
>   	Map-Reduce Framework
>   		Map input records=19
>   		Map output records=19000
>   		Map output bytes=270910
>   		Map output materialized bytes=308922
>   		Input split bytes=202
>   		Combine input records=0
>   		Combine output records=0
>   		Reduce input groups=6988
>   		Reduce shuffle bytes=308922
>   		Reduce input records=19000
>   		Reduce output records=11
>   		Spilled Records=38000
>   		Shuffled Maps =2
>   		Failed Shuffles=0
>   		Merged Map outputs=2
>   		GC time elapsed (ms)=726
>   		CPU time spent (ms)=3960
>   		Physical memory (bytes) snapshot=1149136896
>   		Virtual memory (bytes) snapshot=10374074368
>   		Total committed heap usage (bytes)=1011351552
>   		Peak Map Physical memory (bytes)=451276800
>   		Peak Map Virtual memory (bytes)=3452354560
>   		Peak Reduce Physical memory (bytes)=251953152
>   		Peak Reduce Virtual memory (bytes)=3469680640
>   	Shuffle Errors
>   		BAD_ID=0
>   		CONNECTION=0
>   		IO_ERROR=0
>   		WRONG_LENGTH=0
>   		WRONG_MAP=0
>   		WRONG_REDUCE=0
>   	File Input Format Counters 
>   		Bytes Read=197
>   	File Output Format Counters 
>   		Bytes Written=97
>   bitnami@hadoop:~/code$ hadoop fs -cat /matrix/output/part*
>   2020-05-09 08:32:50,019 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>   0,0,7.0
>   0,1,-43.0
>   0,2,1.0
>   1,0,45.0
>   1,1,-9.0
>   1,2,1.0
>   2,0,59.0
>   2,1,21.0
>   2,2,-1.0
>   3,0,49.0
>   3,1,56.0
> 
> ```

## 2.

* Framework used - PySpark with Apache Spark
* Dataset used
    *Retail.dat*\cite{retailfimi} from fimi repository.
    

* Code and Output - 

```python
! wget http://fimi.uantwerpen.be/data/retail.dat.gz
```

> ``` shell
> 
>     --2020-05-09 18:54:09--  http://fimi.uantwerpen.be/data/retail.dat.gz
>     Resolving fimi.uantwerpen.be (fimi.uantwerpen.be)... 143.129.69.1
>     Connecting to fimi.uantwerpen.be (fimi.uantwerpen.be)|143.129.69.1|:80... connected.
>     HTTP request sent, awaiting response... 200 OK
>     Length: 1636819 (1.6M) [application/x-gzip]
>     Saving to: ‘retail.dat.gz’
>     
>     retail.dat.gz       100%[===================>]   1.56M  1.35MB/s    in 1.2s    
>     
>     2020-05-09 18:54:11 (1.35 MB/s) - ‘retail.dat.gz’ saved [1636819/1636819]
>     
> ```


```python
! gunzip retail.dat.gz
```


```python
from pyspark import SparkContext
sc = SparkContext(appName="FPGrowth")
from pyspark.mllib.fpm import FPGrowth

data = sc.textFile("retail.dat")

transactions = data.map(lambda line: line.strip().split(' '))

model = FPGrowth.train(transactions, minSupport=0.1, numPartitions=100)

result = model.freqItemsets().collect()
for fi in result:
    print(fi)
```

> ``` shell
>     FreqItemset(items=['39'], freq=50675)
>     FreqItemset(items=['48'], freq=42135)
>     FreqItemset(items=['48', '39'], freq=29142)
>     FreqItemset(items=['38'], freq=15596)
>     FreqItemset(items=['38', '39'], freq=10345)
>     FreqItemset(items=['32'], freq=15167)
>     FreqItemset(items=['41'], freq=14945)
>     FreqItemset(items=['41', '48'], freq=9018)
>     FreqItemset(items=['41', '39'], freq=11414)
> ```

## 3. 

* Dataset used
   Synthetic 2-d data dataset\cite{Asets} with 7500 records and 50 centroids.
 

* output obtained after 10 iterations

> ```
> 1 54099.305555555555 43422.47222222222
> 2 26812.17288135593 45042.51525423729
> 3 46427.391304347824 46033.608695652176
> 4 56542.5 37678.7
> 5 52268.153846153844 49461.21153846154
> 6 31411.778925619834 57316.95661157025
> 7 60243.60264900662 27529.953642384105
> 8 16338.80353200883 57026.752759381896
> 9 57887.9 59083.573333333334
> 10 52756.05882352941 45173.58823529412
> 11 45088.660130718956 57134.74183006536
> 12 54118.86046511628 35290.53488372093
> 13 57250.375 43248.625
> 14 43647.43243243243 34811.09459459459
> 15 58804.43859649123 46693.82456140351
> 16 53417.57142857143 42006.75
> 17 51674.48 41968.12
> 18 50238.9875 48869.3375
> 19 61144.03409090909 45597.181818181816
> 20 39338.051546391755 46439.38487972508
> 21 55069.851851851854 41316.51851851852
> 22 59600.17006802721 51800.39455782313
> 23 56852.606741573036 34768.47191011236
> 24 53259.0 40117.36363636364
> 25 40498.0 17797.285714285714
> 26 40832.8 15541.9
> 27 45460.83448275862 26152.110344827586
> 28 37239.09090909091 16667.545454545456
> 29 13951.972292191436 41136.103274559195
> 30 35631.206896551725 38335.73399014778
> 31 41387.53333333333 18374.8
> 32 27042.074324324323 15551.81081081081
> 33 33686.4 19394.5
> 34 39936.47619047619 19550.333333333332
> 35 15052.48510638298 14873.063829787234
> 36 37608.833333333336 20139.833333333332
> 37 29503.13157894737 10930.736842105263
> 38 38156.333333333336 18237.555555555555
> 39 58213.13504823151 15456.639871382637
> 40 39806.642857142855 16668.928571428572
> 41 39201.666666666664 17884.14285714286
> 42 49099.98058252427 11628.757281553399
> 43 38742.77777777778 16266.222222222223
> 44 39895.71428571428 18084.714285714286
> 45 40347.35294117647 29178.928104575163
> 46 35953.0 10834.35
> 47 42220.77777777778 23317.222222222223
> 48 50965.56756756757 23385.344594594593
> 49 29432.983240223464 22525.050279329607
> 50 18996.62153846154 27091.113846153847
> ```

* Log

> ``` shell
>     bitnami@hd2:~/code/kmeans_mapreduce$ sh run.sh
>     2020-05-09 17:56:21,168 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:56:21,304 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     Deleted /KMeans/Resources/Output/1
>     ####################
>     input=/KMeans/Resources/Input/points.txt
>     state=/KMeans/Resources/Input/clusters.txt
>     output=/KMeans/Resources/Output
>     max=10
>     number=3
>     delta=10
>     distance=eucl
>     ####################
>     First iteration adding /KMeans/Resources/Input/clusters.txt
>     2020-05-09 17:56:29,788 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:56:30,253 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0007
>     2020-05-09 17:56:30,444 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:56:30,944 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:56:30,979 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:56:31,004 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:56:31,015 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:56:31,207 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:56:31,253 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0007
>     2020-05-09 17:56:31,254 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:56:31,493 INFO conf.Configuration: resource-types.xml not found
>     2020-05-09 17:56:31,493 INFO resource.ResourceUtils: Unable to find 'resource-types.xml'.
>     2020-05-09 17:56:31,554 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0007
>     2020-05-09 17:56:31,599 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0007/
>     2020-05-09 17:56:31,600 INFO mapreduce.Job: Running job: job_1589044631072_0007
>     2020-05-09 17:56:40,801 INFO mapreduce.Job: Job job_1589044631072_0007 running in uber mode : false
>     2020-05-09 17:56:40,803 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:56:46,859 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:56:55,934 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 17:56:56,944 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:57:01,983 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:57:02,997 INFO mapreduce.Job: Job job_1589044631072_0007 completed successfully
>     2020-05-09 17:57:03,087 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=921233
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121062
>     		HDFS: Number of bytes written=1487
>     		HDFS: Number of read operations=19
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7874
>     		Total time spent by all reduces in occupied slots (ms)=33044
>     		Total time spent by all map tasks (ms)=3937
>     		Total time spent by all reduce tasks (ms)=16522
>     		Total vcore-milliseconds taken by all map tasks=3937
>     		Total vcore-milliseconds taken by all reduce tasks=16522
>     		Total megabyte-milliseconds taken by all map tasks=8062976
>     		Total megabyte-milliseconds taken by all reduce tasks=33837056
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=375795
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=478
>     		CPU time spent (ms)=4430
>     		Physical memory (bytes) snapshot=941056000
>     		Virtual memory (bytes) snapshot=13844869120
>     		Total committed heap usage (bytes)=803209216
>     		Peak Map Physical memory (bytes)=360955904
>     		Peak Map Virtual memory (bytes)=3454283776
>     		Peak Reduce Physical memory (bytes)=194682880
>     		Peak Reduce Virtual memory (bytes)=3463843840
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=50
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1487
>     Counter value = 50
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/1/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/1/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/1/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/1/part-r-00002
>     2020-05-09 17:57:03,169 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:57:08,545 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0008
>     2020-05-09 17:57:08,573 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:09,365 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:57:09,386 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:09,807 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:09,819 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:57:09,870 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:09,922 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0008
>     2020-05-09 17:57:09,922 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:57:09,948 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0008
>     2020-05-09 17:57:09,953 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0008/
>     2020-05-09 17:57:09,953 INFO mapreduce.Job: Running job: job_1589044631072_0008
>     2020-05-09 17:57:21,117 INFO mapreduce.Job: Job job_1589044631072_0008 running in uber mode : false
>     2020-05-09 17:57:21,117 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:57:27,181 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:57:36,261 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 17:57:37,268 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:57:40,285 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:57:41,297 INFO mapreduce.Job: Job job_1589044631072_0008 completed successfully
>     2020-05-09 17:57:41,334 INFO mapreduce.Job: Counters: 55
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121608
>     		HDFS: Number of bytes written=1570
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7918
>     		Total time spent by all reduces in occupied slots (ms)=29400
>     		Total time spent by all map tasks (ms)=3959
>     		Total time spent by all reduce tasks (ms)=14700
>     		Total vcore-milliseconds taken by all map tasks=3959
>     		Total vcore-milliseconds taken by all reduce tasks=14700
>     		Total megabyte-milliseconds taken by all map tasks=8108032
>     		Total megabyte-milliseconds taken by all reduce tasks=30105600
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376079
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=524
>     		CPU time spent (ms)=4670
>     		Physical memory (bytes) snapshot=1068150784
>     		Virtual memory (bytes) snapshot=13849677824
>     		Total committed heap usage (bytes)=944242688
>     		Peak Map Physical memory (bytes)=450019328
>     		Peak Map Virtual memory (bytes)=3457081344
>     		Peak Reduce Physical memory (bytes)=232464384
>     		Peak Reduce Virtual memory (bytes)=3466760192
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=48
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1570
>     Counter value = 48
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/2/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/2/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/2/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/2/part-r-00002
>     2020-05-09 17:57:41,378 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:57:41,838 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0009
>     2020-05-09 17:57:41,859 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:42,181 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:57:42,205 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:42,627 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:42,636 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:57:42,661 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:57:43,073 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0009
>     2020-05-09 17:57:43,073 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:57:43,101 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0009
>     2020-05-09 17:57:43,104 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0009/
>     2020-05-09 17:57:43,104 INFO mapreduce.Job: Running job: job_1589044631072_0009
>     2020-05-09 17:57:57,231 INFO mapreduce.Job: Job job_1589044631072_0009 running in uber mode : false
>     2020-05-09 17:57:57,231 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:58:03,286 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:58:12,346 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:58:16,371 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:58:17,380 INFO mapreduce.Job: Job job_1589044631072_0009 completed successfully
>     2020-05-09 17:58:17,415 INFO mapreduce.Job: Counters: 55
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121691
>     		HDFS: Number of bytes written=1708
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7950
>     		Total time spent by all reduces in occupied slots (ms)=28618
>     		Total time spent by all map tasks (ms)=3975
>     		Total time spent by all reduce tasks (ms)=14309
>     		Total vcore-milliseconds taken by all map tasks=3975
>     		Total vcore-milliseconds taken by all reduce tasks=14309
>     		Total megabyte-milliseconds taken by all map tasks=8140800
>     		Total megabyte-milliseconds taken by all reduce tasks=29304832
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376373
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=540
>     		CPU time spent (ms)=4560
>     		Physical memory (bytes) snapshot=1069543424
>     		Virtual memory (bytes) snapshot=13846642688
>     		Total committed heap usage (bytes)=939524096
>     		Peak Map Physical memory (bytes)=453734400
>     		Peak Map Virtual memory (bytes)=3457204224
>     		Peak Reduce Physical memory (bytes)=228544512
>     		Peak Reduce Virtual memory (bytes)=3463274496
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=45
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1708
>     Counter value = 45
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/3/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/3/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/3/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/3/part-r-00002
>     2020-05-09 17:58:17,475 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:58:17,732 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0010
>     2020-05-09 17:58:17,757 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:17,986 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:58:18,006 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:18,025 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:18,031 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:58:18,057 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:18,466 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0010
>     2020-05-09 17:58:18,466 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:58:18,702 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0010
>     2020-05-09 17:58:18,707 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0010/
>     2020-05-09 17:58:18,707 INFO mapreduce.Job: Running job: job_1589044631072_0010
>     2020-05-09 17:58:32,832 INFO mapreduce.Job: Job job_1589044631072_0010 running in uber mode : false
>     2020-05-09 17:58:32,832 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:58:38,872 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:58:46,918 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 17:58:47,926 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:58:50,942 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:58:51,953 INFO mapreduce.Job: Job job_1589044631072_0010 completed successfully
>     2020-05-09 17:58:51,998 INFO mapreduce.Job: Counters: 55
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121829
>     		HDFS: Number of bytes written=1705
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7862
>     		Total time spent by all reduces in occupied slots (ms)=27910
>     		Total time spent by all map tasks (ms)=3931
>     		Total time spent by all reduce tasks (ms)=13955
>     		Total vcore-milliseconds taken by all map tasks=3931
>     		Total vcore-milliseconds taken by all reduce tasks=13955
>     		Total megabyte-milliseconds taken by all map tasks=8050688
>     		Total megabyte-milliseconds taken by all reduce tasks=28579840
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376501
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=529
>     		CPU time spent (ms)=4450
>     		Physical memory (bytes) snapshot=1033535488
>     		Virtual memory (bytes) snapshot=13844987904
>     		Total committed heap usage (bytes)=918552576
>     		Peak Map Physical memory (bytes)=455417856
>     		Peak Map Virtual memory (bytes)=3456450560
>     		Peak Reduce Physical memory (bytes)=195698688
>     		Peak Reduce Virtual memory (bytes)=3463507968
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=44
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1705
>     Counter value = 44
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/4/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/4/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/4/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/4/part-r-00002
>     2020-05-09 17:58:52,049 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:58:53,547 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0011
>     2020-05-09 17:58:53,567 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:53,800 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:58:53,824 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:53,850 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:53,860 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:58:53,896 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:58:53,913 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0011
>     2020-05-09 17:58:53,914 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:58:54,149 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0011
>     2020-05-09 17:58:54,152 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0011/
>     2020-05-09 17:58:54,152 INFO mapreduce.Job: Running job: job_1589044631072_0011
>     2020-05-09 17:59:07,259 INFO mapreduce.Job: Job job_1589044631072_0011 running in uber mode : false
>     2020-05-09 17:59:07,259 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:59:13,299 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:59:20,333 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 17:59:22,344 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:59:25,358 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:59:25,367 INFO mapreduce.Job: Job job_1589044631072_0011 completed successfully
>     2020-05-09 17:59:25,402 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121826
>     		HDFS: Number of bytes written=1789
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7430
>     		Total time spent by all reduces in occupied slots (ms)=29566
>     		Total time spent by all map tasks (ms)=3715
>     		Total time spent by all reduce tasks (ms)=14783
>     		Total vcore-milliseconds taken by all map tasks=3715
>     		Total vcore-milliseconds taken by all reduce tasks=14783
>     		Total megabyte-milliseconds taken by all map tasks=7608320
>     		Total megabyte-milliseconds taken by all reduce tasks=30275584
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376662
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=383
>     		CPU time spent (ms)=4450
>     		Physical memory (bytes) snapshot=987000832
>     		Virtual memory (bytes) snapshot=13853597696
>     		Total committed heap usage (bytes)=826802176
>     		Peak Map Physical memory (bytes)=363061248
>     		Peak Map Virtual memory (bytes)=3456167936
>     		Peak Reduce Physical memory (bytes)=231530496
>     		Peak Reduce Virtual memory (bytes)=3466874880
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=45
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1789
>     Counter value = 45
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/5/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/5/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/5/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/5/part-r-00002
>     2020-05-09 17:59:25,436 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 17:59:25,455 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0012
>     2020-05-09 17:59:25,469 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:59:25,741 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 17:59:25,769 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:59:25,788 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:59:25,793 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 17:59:25,825 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 17:59:25,842 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0012
>     2020-05-09 17:59:25,843 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 17:59:25,868 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0012
>     2020-05-09 17:59:25,870 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0012/
>     2020-05-09 17:59:25,871 INFO mapreduce.Job: Running job: job_1589044631072_0012
>     2020-05-09 17:59:39,980 INFO mapreduce.Job: Job job_1589044631072_0012 running in uber mode : false
>     2020-05-09 17:59:39,980 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 17:59:46,021 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 17:59:54,085 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 17:59:56,094 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 17:59:59,110 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 17:59:59,116 INFO mapreduce.Job: Job job_1589044631072_0012 completed successfully
>     2020-05-09 17:59:59,151 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121910
>     		HDFS: Number of bytes written=1828
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7922
>     		Total time spent by all reduces in occupied slots (ms)=30030
>     		Total time spent by all map tasks (ms)=3961
>     		Total time spent by all reduce tasks (ms)=15015
>     		Total vcore-milliseconds taken by all map tasks=3961
>     		Total vcore-milliseconds taken by all reduce tasks=15015
>     		Total megabyte-milliseconds taken by all map tasks=8112128
>     		Total megabyte-milliseconds taken by all reduce tasks=30750720
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376881
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=572
>     		CPU time spent (ms)=4490
>     		Physical memory (bytes) snapshot=1031585792
>     		Virtual memory (bytes) snapshot=13844811776
>     		Total committed heap usage (bytes)=922746880
>     		Peak Map Physical memory (bytes)=451375104
>     		Peak Map Virtual memory (bytes)=3454791680
>     		Peak Reduce Physical memory (bytes)=194048000
>     		Peak Reduce Virtual memory (bytes)=3463921664
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=46
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1828
>     Counter value = 46
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/6/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/6/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/6/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/6/part-r-00002
>     2020-05-09 17:59:59,194 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 18:00:00,152 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0013
>     2020-05-09 18:00:00,169 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:00,402 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 18:00:00,416 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:00,435 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:00,441 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 18:00:00,464 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:00,478 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0013
>     2020-05-09 18:00:00,478 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 18:00:00,500 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0013
>     2020-05-09 18:00:00,504 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0013/
>     2020-05-09 18:00:00,504 INFO mapreduce.Job: Running job: job_1589044631072_0013
>     2020-05-09 18:00:15,612 INFO mapreduce.Job: Job job_1589044631072_0013 running in uber mode : false
>     2020-05-09 18:00:15,612 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 18:00:21,660 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 18:00:29,707 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 18:00:31,721 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 18:00:33,731 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 18:00:34,741 INFO mapreduce.Job: Job job_1589044631072_0013 completed successfully
>     2020-05-09 18:00:34,773 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121949
>     		HDFS: Number of bytes written=1780
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=8140
>     		Total time spent by all reduces in occupied slots (ms)=28786
>     		Total time spent by all map tasks (ms)=4070
>     		Total time spent by all reduce tasks (ms)=14393
>     		Total vcore-milliseconds taken by all map tasks=4070
>     		Total vcore-milliseconds taken by all reduce tasks=14393
>     		Total megabyte-milliseconds taken by all map tasks=8335360
>     		Total megabyte-milliseconds taken by all reduce tasks=29476864
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376957
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=533
>     		CPU time spent (ms)=4380
>     		Physical memory (bytes) snapshot=1017503744
>     		Virtual memory (bytes) snapshot=13839163392
>     		Total committed heap usage (bytes)=863502336
>     		Peak Map Physical memory (bytes)=450199552
>     		Peak Map Virtual memory (bytes)=3454394368
>     		Peak Reduce Physical memory (bytes)=193122304
>     		Peak Reduce Virtual memory (bytes)=3463684096
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=45
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1780
>     Counter value = 45
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/7/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/7/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/7/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/7/part-r-00002
>     2020-05-09 18:00:34,815 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 18:00:35,990 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0014
>     2020-05-09 18:00:36,004 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:36,253 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 18:00:36,275 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:36,306 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:36,313 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 18:00:36,333 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:00:36,340 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0014
>     2020-05-09 18:00:36,340 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 18:00:36,364 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0014
>     2020-05-09 18:00:36,372 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0014/
>     2020-05-09 18:00:36,372 INFO mapreduce.Job: Running job: job_1589044631072_0014
>     2020-05-09 18:00:49,480 INFO mapreduce.Job: Job job_1589044631072_0014 running in uber mode : false
>     2020-05-09 18:00:49,480 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 18:00:55,539 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 18:01:04,594 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 18:01:05,598 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 18:01:08,617 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 18:01:09,628 INFO mapreduce.Job: Job job_1589044631072_0014 completed successfully
>     2020-05-09 18:01:09,659 INFO mapreduce.Job: Counters: 55
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121901
>     		HDFS: Number of bytes written=1795
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=8250
>     		Total time spent by all reduces in occupied slots (ms)=28742
>     		Total time spent by all map tasks (ms)=4125
>     		Total time spent by all reduce tasks (ms)=14371
>     		Total vcore-milliseconds taken by all map tasks=4125
>     		Total vcore-milliseconds taken by all reduce tasks=14371
>     		Total megabyte-milliseconds taken by all map tasks=8448000
>     		Total megabyte-milliseconds taken by all reduce tasks=29431808
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376971
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=511
>     		CPU time spent (ms)=4260
>     		Physical memory (bytes) snapshot=955428864
>     		Virtual memory (bytes) snapshot=13851729920
>     		Total committed heap usage (bytes)=805306368
>     		Peak Map Physical memory (bytes)=366051328
>     		Peak Map Virtual memory (bytes)=3457429504
>     		Peak Reduce Physical memory (bytes)=199876608
>     		Peak Reduce Virtual memory (bytes)=3467018240
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=40
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1795
>     Counter value = 40
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/8/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/8/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/8/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/8/part-r-00002
>     2020-05-09 18:01:09,693 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 18:01:09,706 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0015
>     2020-05-09 18:01:09,718 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:09,969 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 18:01:09,988 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:10,013 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:10,025 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 18:01:10,045 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:12,048 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0015
>     2020-05-09 18:01:12,048 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 18:01:12,284 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0015
>     2020-05-09 18:01:12,287 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0015/
>     2020-05-09 18:01:12,287 INFO mapreduce.Job: Running job: job_1589044631072_0015
>     2020-05-09 18:01:23,400 INFO mapreduce.Job: Job job_1589044631072_0015 running in uber mode : false
>     2020-05-09 18:01:23,401 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 18:01:29,438 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 18:01:37,480 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 18:01:39,493 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 18:01:42,506 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 18:01:44,520 INFO mapreduce.Job: Job job_1589044631072_0015 completed successfully
>     2020-05-09 18:01:44,564 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923217
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121916
>     		HDFS: Number of bytes written=1790
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7878
>     		Total time spent by all reduces in occupied slots (ms)=29930
>     		Total time spent by all map tasks (ms)=3939
>     		Total time spent by all reduce tasks (ms)=14965
>     		Total vcore-milliseconds taken by all map tasks=3939
>     		Total vcore-milliseconds taken by all reduce tasks=14965
>     		Total megabyte-milliseconds taken by all map tasks=8067072
>     		Total megabyte-milliseconds taken by all reduce tasks=30648320
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376981
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=512
>     		CPU time spent (ms)=4360
>     		Physical memory (bytes) snapshot=971091968
>     		Virtual memory (bytes) snapshot=13843107840
>     		Total committed heap usage (bytes)=830996480
>     		Peak Map Physical memory (bytes)=361992192
>     		Peak Map Virtual memory (bytes)=3457384448
>     		Peak Reduce Physical memory (bytes)=230891520
>     		Peak Reduce Virtual memory (bytes)=3463692288
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=40
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1790
>     Counter value = 40
>     Algorithm converged!
>     Checking path /KMeans/Resources/Output/9/part-r-[0-9]*
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/9/part-r-00000
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/9/part-r-00001
>     Adding hdfs://localhost:8020/KMeans/Resources/Output/9/part-r-00002
>     2020-05-09 18:01:44,614 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
>     2020-05-09 18:01:44,627 INFO mapreduce.JobResourceUploader: Disabling Erasure Coding for path: /tmp/hadoop-yarn/staging/hadoop/.staging/job_1589044631072_0016
>     2020-05-09 18:01:44,655 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:45,311 INFO input.FileInputFormat: Total input files to process : 1
>     2020-05-09 18:01:45,330 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:45,347 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:45,354 INFO mapreduce.JobSubmitter: number of splits:1
>     2020-05-09 18:01:45,384 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     2020-05-09 18:01:45,397 INFO mapreduce.JobSubmitter: Submitting tokens for job: job_1589044631072_0016
>     2020-05-09 18:01:45,398 INFO mapreduce.JobSubmitter: Executing with tokens: []
>     2020-05-09 18:01:45,421 INFO impl.YarnClientImpl: Submitted application application_1589044631072_0016
>     2020-05-09 18:01:45,428 INFO mapreduce.Job: The url to track the job: http://localhost:8088/proxy/application_1589044631072_0016/
>     2020-05-09 18:01:45,428 INFO mapreduce.Job: Running job: job_1589044631072_0016
>     2020-05-09 18:01:59,567 INFO mapreduce.Job: Job job_1589044631072_0016 running in uber mode : false
>     2020-05-09 18:01:59,567 INFO mapreduce.Job:  map 0% reduce 0%
>     2020-05-09 18:02:05,601 INFO mapreduce.Job:  map 100% reduce 0%
>     2020-05-09 18:02:13,643 INFO mapreduce.Job:  map 100% reduce 33%
>     2020-05-09 18:02:14,653 INFO mapreduce.Job:  map 100% reduce 67%
>     2020-05-09 18:02:20,680 INFO mapreduce.Job:  map 100% reduce 100%
>     2020-05-09 18:02:20,684 INFO mapreduce.Job: Job job_1589044631072_0016 completed successfully
>     2020-05-09 18:02:20,714 INFO mapreduce.Job: Counters: 56
>     	File System Counters
>     		FILE: Number of bytes read=3459
>     		FILE: Number of bytes written=923225
>     		FILE: Number of read operations=0
>     		FILE: Number of large read operations=0
>     		FILE: Number of write operations=0
>     		HDFS: Number of bytes read=121911
>     		HDFS: Number of bytes written=1805
>     		HDFS: Number of read operations=21
>     		HDFS: Number of large read operations=0
>     		HDFS: Number of write operations=6
>     		HDFS: Number of bytes read erasure-coded=0
>     	Job Counters 
>     		Killed reduce tasks=1
>     		Launched map tasks=1
>     		Launched reduce tasks=3
>     		Data-local map tasks=1
>     		Total time spent by all maps in occupied slots (ms)=7934
>     		Total time spent by all reduces in occupied slots (ms)=32352
>     		Total time spent by all map tasks (ms)=3967
>     		Total time spent by all reduce tasks (ms)=16176
>     		Total vcore-milliseconds taken by all map tasks=3967
>     		Total vcore-milliseconds taken by all reduce tasks=16176
>     		Total megabyte-milliseconds taken by all map tasks=8124416
>     		Total megabyte-milliseconds taken by all reduce tasks=33128448
>     	Map-Reduce Framework
>     		Map input records=7501
>     		Map output records=5652
>     		Map output bytes=376974
>     		Map output materialized bytes=3459
>     		Input split bytes=120
>     		Combine input records=5652
>     		Combine output records=50
>     		Reduce input groups=50
>     		Reduce shuffle bytes=3459
>     		Reduce input records=50
>     		Reduce output records=50
>     		Spilled Records=100
>     		Shuffled Maps =3
>     		Failed Shuffles=0
>     		Merged Map outputs=3
>     		GC time elapsed (ms)=571
>     		CPU time spent (ms)=4540
>     		Physical memory (bytes) snapshot=1139503104
>     		Virtual memory (bytes) snapshot=13848748032
>     		Total committed heap usage (bytes)=993525760
>     		Peak Map Physical memory (bytes)=446189568
>     		Peak Map Virtual memory (bytes)=3455320064
>     		Peak Reduce Physical memory (bytes)=233857024
>     		Peak Reduce Virtual memory (bytes)=3465424896
>     	Shuffle Errors
>     		BAD_ID=0
>     		CONNECTION=0
>     		IO_ERROR=0
>     		WRONG_LENGTH=0
>     		WRONG_MAP=0
>     		WRONG_REDUCE=0
>     	enums.KMeansCounter
>     		ADJUSTED=40
>     	File Input Format Counters 
>     		Bytes Read=120001
>     	File Output Format Counters 
>     		Bytes Written=1805
>     Algorithm converged max iterations reached
>     Algorithm converged!
>     2020-05-09 18:02:26,285 INFO sasl.SaslDataTransferClient: SASL encryption trust check: localHostTrusted = false, remoteHostTrusted = false
>     1 54099.305555555555 43422.47222222222
>     2 26812.17288135593 45042.51525423729
>     3 46427.391304347824 46033.608695652176
>     4 56542.5 37678.7
>     5 52268.153846153844 49461.21153846154
>     6 31411.778925619834 57316.95661157025
>     7 60243.60264900662 27529.953642384105
>     8 16338.80353200883 57026.752759381896
>     9 57887.9 59083.573333333334
>     10 52756.05882352941 45173.58823529412
>     11 45088.660130718956 57134.74183006536
>     12 54118.86046511628 35290.53488372093
>     13 57250.375 43248.625
>     14 43647.43243243243 34811.09459459459
>     15 58804.43859649123 46693.82456140351
>     16 53417.57142857143 42006.75
>     17 51674.48 41968.12
>     18 50238.9875 48869.3375
>     19 61144.03409090909 45597.181818181816
>     20 39338.051546391755 46439.38487972508
>     21 55069.851851851854 41316.51851851852
>     22 59600.17006802721 51800.39455782313
>     23 56852.606741573036 34768.47191011236
>     24 53259.0 40117.36363636364
>     25 40498.0 17797.285714285714
>     26 40832.8 15541.9
>     27 45460.83448275862 26152.110344827586
>     28 37239.09090909091 16667.545454545456
>     29 13951.972292191436 41136.103274559195
>     30 35631.206896551725 38335.73399014778
>     31 41387.53333333333 18374.8
>     32 27042.074324324323 15551.81081081081
>     33 33686.4 19394.5
>     34 39936.47619047619 19550.333333333332
>     35 15052.48510638298 14873.063829787234
>     36 37608.833333333336 20139.833333333332
>     37 29503.13157894737 10930.736842105263
>     38 38156.333333333336 18237.555555555555
>     39 58213.13504823151 15456.639871382637
>     40 39806.642857142855 16668.928571428572
>     41 39201.666666666664 17884.14285714286
>     42 49099.98058252427 11628.757281553399
>     43 38742.77777777778 16266.222222222223
>     44 39895.71428571428 18084.714285714286
>     45 40347.35294117647 29178.928104575163
>     46 35953.0 10834.35
>     47 42220.77777777778 23317.222222222223
>     48 50965.56756756757 23385.344594594593
>     49 29432.983240223464 22525.050279329607
>     50 18996.62153846154 27091.113846153847
> 
> ```
