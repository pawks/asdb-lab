---
title: "Exploratory Assignment"
author: [S Pawan Kumar, CED16I043]
date: "2020-05-10"
titlepage: true
table-use-row-colors: true
classoption: [oneside]
...

# Hyperlink Induced Topic Search(HITS)

Hyperlink-Induced Topic Search (HITS; also known as hubs and authorities) is a link analysis algorithm that rates Web pages, developed by Jon Kleinberg. The idea behind Hubs and Authorities stemmed from a particular insight into the creation of web pages when the Internet was originally forming; that is, certain web pages, known as hubs, served as large directories that were not actually authoritative in the information that they held, but were used as compilations of a broad catalog of information that led users direct to other authoritative pages. In other words, a good hub represents a page that pointed to many other pages, while a good authority represents a page that is linked by many different hubs.The scheme therefore assigns two scores for each page: its authority, which estimates the value of the content of the page, and its hub value, which estimates the value of its links to other pages.

The fact that the hubbiness of a page is proportional to the sum of the authority of its successors is expressed by the equation $h =\lambda L a$, where $\lambda$ is an unknown constant representing the scaling factor needed. Likewise, the fact that the authority of a page is proportional to the sum of the hubbinesses of its predecessors is expressed by $a = \mu L^{\mathrm{T}} h$, where $\mu$ is another scaling constant. These equations allow us to compute the hubbiness and authority independently, by substituting one equation in the other, as:

\begin{equation}\mathbf{h}=\lambda \mu L L^{\mathrm{T}} \mathbf{h}\end{equation}

\begin{equation}\mathbf{a}=\lambda \mu L^{\mathrm{T}} L \mathbf{a}\end{equation}

However, since LL T and L T L are not as sparse as L and L T , we are usually
better off computing h and a in a true mutual recursion. That is, start with h
a vector of all 1’s.   
1. Compute $a = L^{\mathrm{T}}h$ and then scale so the largest component is 1.
2. Next, compute $h = La$ and scale again.

## Important concepts
* Authority - An estimate of the value of the content of the page. 
* Hub - An estimate of the value of its links to other pages. 

## Pseudocode

```
G := set of pages
for each page p in G do
    p.auth = 1 // p.auth is the authority score of the page p
    p.hub = 1 // p.hub is the hub score of the page p
    for step from 1 to k do // run the algorithm for k steps
        norm = 0
        for each page p in G do  // update all authority values first
            p.auth = 0
            for each page q in p.incomingNeighbors do // p.incomingNeighbors is the set of pages that link to p
                p.auth += q.hub
            norm += square(p.auth) // calculate the sum of the squared auth values to normalise
         norm = sqrt(norm)
         for each page p in G do  // update the auth scores 
             p.auth = p.auth / norm  // normalise the auth values
         norm = 0
         for each page p in G do  // then update all hub values
             p.hub = 0
             for each page r in p.outgoingNeighbors do // p.outgoingNeighbors is the set of pages that p links to
                 p.hub += r.auth
             norm += square(p.hub) // calculate the sum of the squared hub values to normalise
         norm = sqrt(norm)
         for each page p in G do  // then update all hub values
             p.hub = p.hub / norm   // normalise the hub values
```

## Trace

* Sample data  
    ![](hits.png)  
* We assume the scaling function to be division by the largest element in the matrix for simplicity sake.
* Initial Data
\begin{equation}
h=\left[\begin{array}{l}
1 \\ 1 \\ 1 \\ 1 \\ 1 \\
\end{array}\right] \quad L=\left[\begin{array}{lllll}
0 & 1 & 1 & 1 & 0 \\
1 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 1 \\
0 & 1 & 1 & 0 & 0 \\
0 & 0 & 0 & 0 & 0
\end{array}\right] \quad L^{\mathrm{T}}=\left[\begin{array}{lllll}
0 & 1 & 0 & 0 & 0 \\
1 & 0 & 0 & 1 & 0 \\
1 & 0 & 0 & 1 & 0 \\
1 & 1 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0
\end{array}\right]\end{equation}
* k = 1
\begin{equation}\begin{aligned}
L^{\mathrm{T}} h=\left[\begin{array}{l}
1 \\
2 \\
2 \\
2 \\
1
\end{array}\right] \quad a=\left[\begin{array}{c}
1 / 2 \\
1 \\
1 \\
1 \\
1 / 2
\end{array}\right] \quad L a=\left[\begin{array}{c}
3 \\
3 / 2 \\
1 / 2 \\
2 \\
0
\end{array}\right] \quad h=\left[\begin{array}{c}
1 \\
1 / 2 \\
1 / 6 \\
2 / 3 \\
0
\end{array}\right]
\end{aligned}\end{equation}
* k = 2
\begin{equation}\begin{aligned}
L^{\mathrm{T}} h=\left[\begin{array}{c}
1 / 2 \\
5 / 3 \\
5 / 3 \\
3 / 2 \\
1 / 6
\end{array}\right] \quad a=\left[\begin{array}{c}
3 / 10 \\
1 \\
1 \\
9 / 10 \\
1 / 10
\end{array}\right] \quad L a=\left[\begin{array}{c}
29 / 10 \\
6 / 5 \\
1 / 10 \\
2 \\
0
\end{array}\right] \quad h=\left[\begin{array}{c}
1 \\
12 / 29 \\
1 / 29 \\
20 / 29 \\
0
\end{array}\right]
\end{aligned}\end{equation}

* **Ranking:**
    * Authority - {2,3,4,1,5}
    * Hub - {1,3,2,4,5}

# Clustering Algorithms

## BIRCH
BIRCH (balanced iterative reducing and clustering using hierarchies) is an unsupervised data mining algorithm used to perform hierarchical clustering over particularly large datasets. An advantage of BIRCH is its ability to incrementally and dynamically cluster incoming, multi-dimensional metric data points in an attempt to produce the best quality clustering for a given set of resources (memory and time constraints). In most cases, BIRCH only requires a single scan of the database.

### Problems with previous methods
Previous clustering algorithms performed less effectively over very large databases and did not adequately consider the case wherein a data-set was too large to fit in main memory. As a result, there was a lot of overhead maintaining high clustering quality while minimizing the cost of additional IO (input/output) operations. Furthermore, most of BIRCH's predecessors inspect all data points (or all currently existing clusters) equally for each 'clustering decision' and do not perform heuristic weighting based on the distance between these data points. 

### Advantages with BIRCH
It is local in that each clustering decision is made without scanning all data points and currently existing clusters. It exploits the observation that the data space is not usually uniformly occupied and not every data point is equally important. It makes full use of available memory to derive the finest possible sub-clusters while minimizing I/O costs. It is also an incremental method that does not require the whole data set in advance. 

### Algorithm
The BIRCH algorithm takes as input a set of N data points, represented as real-valued vectors, and a desired number of clusters K. It operates in four phases, the second of which is optional. 

1. The first phase builds a clustering feature(**CF**) tree out of the data points, a height-balanced tree data structure, defined as follows:
    * Given a set of _N_ d-dimensional data points, the clustering feature **CF** of the set is defined as the triple $CF=(N, \overrightarrow{LS}, S S)$, where $\overrightarrow{L S}=\sum_{i=1}^{N} \overrightarrow{X_{i}}$ is the linear sum and $SS=\sum_{i=1}^{N} \overrightarrow{X_{i}^2}$ is the square sum of data points.
    * Clustering features are organized in a CF tree, a height-balanced tree with two parameters: branching factor _B_ and threshold _T_. Each non-leaf node contains at most _B_ entries of the form [CF\textsubscript{i} , child\textsubscript{i}], where child\textsubscript{i} is a pointer to its i\textsuperscript{th} child node and $ CF_i $ the clustering feature representing the associated subcluster. A leaf node contains at most *L* entries each of the form [ $CF_{i}$ ] . It also has two pointers prev and next which are used to chain all leaf nodes together. The tree size depends on the parameter *T*. A node is required to fit in a page of size _P_ . _B_ and _L_ are determined by _P_. So _P_ can be varied for performance tuning. It is a very compact representation of the dataset because each entry in a leaf node is not a single data point but a subcluster.
2. In the second step, the algorithm scans all the leaf entries in the initial **CF** tree to rebuild a smaller **CF** tree, while removing outliers and grouping crowded subclusters into larger ones. This step is marked optional in the original presentation of BIRCH. 
3. In step three an existing clustering algorithm is used to cluster all leaf entries. Here an agglomerative hierarchical clustering algorithm is applied directly to the subclusters represented by their **CF** vectors. It also provides the flexibility of allowing the user to specify either the desired number of clusters or the desired diameter threshold for clusters. After this step a set of clusters is obtained that captures major distribution pattern in the data. However, there might exist minor and localized inaccuracies which can be handled by an optional step 4.
4. In step 4 the centroids of the clusters produced in step 3 are used as seeds and redistribute the data points to its closest seeds to obtain a new set of clusters. Step 4 also provides us with an option of discarding outliers. That is a point which is too far from its closest seed can be treated as an outlier. 

### Formulae for calculation
* Centroid
\begin{equation}\vec{C}=\frac{\sum_{i=1}^{N} \vec{X}_{i}}{N}=\frac{\overrightarrow{L S}}{N}\end{equation}
* Radius
\begin{equation}R=\sqrt{\frac{\sum_{i=1}^{N}\left(\vec{X}_{i}-\vec{C}\right)^{2}}{N}}=\sqrt{\frac{N \cdot \vec{C}^{2}+S S-2 \cdot \vec{C} \cdot \overrightarrow{L S}}{N}}=\sqrt{\frac{S S}{N}-\left(\frac{\overrightarrow{L S}}{N}\right)^{2}}\end{equation}
* Average Linkage Distance between 2 clusters $CF_1=(N_1, \overrightarrow{LS_1}, SS_1)$ and $CF_2=(N_2, \overrightarrow{LS_2}, SS_2)$
\begin{equation}D_{2}=\sqrt{\frac{\sum_{i=1}^{N_{1}} \sum_{j=1}^{N_{2}}\left(\vec{X}_{i}-\vec{Y}_{j}\right)^{2}}{N_{1} \cdot N_{2}}}=\sqrt{\frac{N_{1} \cdot S S_{2}+N_{2} \cdot S S_{1}-2 \cdot \overrightarrow{L S_{1}} \cdot \overrightarrow{L S_{2}}}{N_{1} \cdot N_{2}}}\end{equation}
In multidimensional cases the square root should be replaced with a suitable norm. 

### Trace
* Datapoints: (3,4), (2,6), (4,5), (4,7), (3,8)
* N = 5
* NS = (16,30)  i.e. $3+2+4+4+3=16$ and $4+6+5+7+8=30$
* SS = (54,190) i.e. $3^2+2^2+4^2+4^2+3^2=54$ and $4^2+6^2+5^2+7^2+8^2=190$
* CF = < 5, (16,30), (54,190) >

## DBSCAN
Density-based spatial clustering of applications with noise (DBSCAN) is a well-
known data clustering algorithm that is commonly used in data mining and
machine learning. Based on a set of points (let’s think in a bidimensional space
as exemplified in the figure), DBSCAN groups together points that are close to
each other based on a distance measurement (usually Euclidean distance) and
a minimum number of points. It also marks as outliers the points that are in
low-density regions.

### Parameters
* Epsilon/eps/ $ \epsilon $: It defines the neighbourhood around a data point i.e. if the distance between two points is less than or equal to eps then they are considered as neighbours. If the eps value is chosen too small then large part of the data will be considered as outliers. If it is chosen very large then the clusters will merge and majority of the data points will be in the same clusters. One way to find the eps value is based on the k-distance graph.
* Minimun Points/minPts: The minimum number of points to form a dense region. For example, if we set the minPoints parameter as 5, then we need at least 5 points to form a dense region.

### Pseudocode

```
DBSCAN(DB, distFunc, eps, minPts) {
    C = 0                                                  /* Cluster counter */
    for each point P in database DB {
        if label(P) ≠ undefined then continue              /* Previously processed in inner loop */
        Neighbors N = RangeQuery(DB, distFunc, P, eps)     /* Find neighbors */
        if |N| < minPts then {                             /* Density check */
            label(P) = Noise                               /* Label as Noise */
            continue
        }
        C = C + 1                                          /* next cluster label */
        label(P) = C                                       /* Label initial point */
        Seed set S = N \ {P}                               /* Neighbors to expand */
        for each point Q in S {                            /* Process every seed point */
            if label(Q) = Noise then label(Q) = C          /* Change Noise to border point */
            if label(Q) ≠ undefined then continue          /* Previously processed */
            label(Q) = C                                   /* Label neighbor */
            Neighbors N = RangeQuery(DB, distFunc, Q, eps) /* Find neighbors */
            if |N| ≥ minPts then {                         /* Density check */
                S = S ∪ N                                  /* Add new neighbors to seed set */
            }
        }
    }
}
```

where RangeQuery can be implemented using a database index for better performance, or using a slow linear scan: 

```
RangeQuery(DB, distFunc, Q, eps) {
    Neighbors = empty list
    for each point P in database DB {                      /* Scan all points in the database */
        if distFunc(Q, P) ≤ eps then {                     /* Compute distance and check epsilon */
            Neighbors = Neighbors ∪ {P}                    /* Add to result */
        }
    }
    return Neighbors
}

```

### Trace

* Dataset - (5,1), (3,4), (7,3), (3,8), (3,6), (4,7), (4,5), (4,5), (5,5), (7,5), (8,5)
* $\epsilon=2$ and minPts=3 and we use Eucledian distance measure.
* Neighbourhood Table

|  Point  |  Neighbour                         |  Label   |
|---------|------------------------------------|----------|
|    (5,1)|      -                             |   Noise  |
|   (3,4) |     (3,6),(4,5)                    |   Noise  |
|   (7,3) |    (7,5)                           |    Noise |
|    (3,8)|     (3,6),(4,7)                    |    Noise |
|   (3,6) |     (3,4),(3,8),(4,7),(4,5)        |   Core   |
|   (4,7) |           (3,8),(3,6),(4,5)        |  Core    |
|   (4,5) |             (3,4),(3,6),(4,7),(5,5)|   Core   |
|    (5,5)|              (4,5),(7,5)           |   Noise  |
|    (7,5)|               (7,3),(5,5),(8,5)    |   Core   |
|    (8,5)|                (7,5)               |    Noise |

* Form Clusters
$C_1$ = {(3, 4), (3, 8), (3, 6), (4, 7), (4, 5)}
If it is labelled as a core point, then it remains unchanged. If it is labelled as noise then change the label to border point, updating the cluster when required.
    * (3,4): Border Point
    * (3,8): Border Point
    * (4,7): Core Point
    * (4,5): Core Point, $C_1$ = {(3, 4), (3, 8), (3, 6), (4, 7), (4, 5), (5, 5)}
    * (5,5): Core Point, $C_1$ =  {(3, 4), (3, 8), (3, 6), (4, 7), (4, 5), (5, 5), (7, 5)}
    * (7,5): $C_1$ = {(3, 4), (3, 8), (3, 6), (4, 7), (4, 5), (5, 5), (7, 5), (7, 3), (8, 5)}
    * (7,3): Border Point
    * (8,5): Border Point
* No core points are left unvisited.
* Cluster Details - 
    $C_1$ = {(3, 4), (3, 8), (3, 6), (4, 7), (4, 5), (5, 5), (7, 5), (7, 3), (8, 5)}
    $Noise = {(5, 1)}$

# Operators in GA
## Selection
During each successive generation, a portion of the existing population is selected to breed a new generation. Individual solutions are selected through a fitness-based process, where fitter solutions (as measured by a fitness function) are typically more likely to be selected. Certain selection methods rate the fitness of each solution and preferentially select the best solutions. Other methods rate only a random sample of the population, as the former process may be very time-consuming. The fitness function is defined over the genetic representation and measures the quality of the represented solution. The fitness function is always problem dependent.

* Roulette Wheel Selection - Consider a circular wheel. The wheel is divided into n pies, where n is the number of individuals in the population. Each individual gets a portion of the circle which is proportional to its fitness value. A fixed point is chosen on the wheel circumference as shown and the wheel is rotated. The region of the wheel which comes in front of the fixed point is chosen as the parent. For the second parent, the same process is repeated. It is clear that a fitter individual has a greater pie on the wheel and therefore a greater chance of landing in front of the fixed point when the wheel is rotated. Therefore, the probability of choosing an individual depends directly on its fitness. Implementation wise, we use the following steps −

    * Calculate S = the sum of a finesses.
    * Generate a random number between 0 and S.
    * Starting from the top of the population, keep adding the finesses to the partial sum P, till $P<S$.
    * The individual for which P exceeds S is the chosen individual.

    ![](rws.png)  
* Stochastic Universal selection - Stochastic Universal Sampling is quite similar to Roulette wheel selection, however instead of having just one fixed point, we have multiple fixed points as shown in the following image. Therefore, all the parents are chosen in just one spin of the wheel. Also, such a setup encourages the highly fit individuals to be chosen at least once.  
    ![](sus.png)  
* Tournament Selection - In K-Way tournament selection, we select K individuals from the population at random and select the best out of these to become a parent. The same process is repeated for selecting the next parent. Tournament Selection is also extremely popular in literature as it can even work with negative fitness values.   
    ![](ts.png)  
* Random Selection - In this strategy we randomly select parents from the existing population. There is no selection pressure towards fitter individuals and therefore this strategy is usually avoided. 

## Crossover

In genetic algorithms and evolutionary computation, crossover, also called recombination, is a genetic operator used to combine the genetic information of two parents to generate new offspring. It is one way to stochastically generate new solutions from an existing population, and analogous to the crossover that happens during sexual reproduction in biology. Solutions can also be generated by cloning an existing solution, which is analogous to asexual reproduction. Newly generated solutions are typically mutated before being added to the population. Different variants of crossovers are - 

* Single point crossover - A point on both parents' chromosomes is picked randomly, and designated a 'crossover point'. Bits to the right of that point are swapped between the two parent chromosomes. This results in two offspring, each carrying some genetic information from both parents.  
    ![](spc.png)
* Two point and k-point crossover - In two-point crossover, two crossover points are picked randomly from the parent chromosomes. The bits in between the two points are swapped between the parent organisms.  
    ![](tpc.png)
* Uniform crossover - In uniform crossover, typically, each bit is chosen from either parent with equal probability. Other mixing ratios are sometimes used, resulting in offspring which inherit more genetic information from one parent than the other.   
    ![](uc.png)
* Davis’ Order Crossover (OX1) - OX1 is used for permutation based crossovers with the intention of transmitting information about relative ordering to the off-springs. It works as follows −

    * Create two random crossover points in the parent and copy the segment between them from the first parent to the first offspring.
    * Now, starting from the second crossover point in the second parent, copy the remaining unused numbers from the second parent to the first child, wrapping around the list.
    * Repeat for the second child with the parent’s role reversed.

    ![](doc.png)

## Mutation

Mutation is a genetic operator used to maintain genetic diversity from one generation of a population of genetic algorithm chromosomes to the next. It is analogous to biological mutation. Mutation alters one or more gene values in a chromosome from its initial state. In mutation, the solution may change entirely from the previous solution. Hence GA can come to a better solution by using mutation. Mutation occurs during evolution according to a user-definable mutation probability. This probability should be set low. If it is set too high, the search will turn into a primitive random search. Different variants of the mutation operator are - 

* Bit string Mutation - The mutation of bit strings ensue through bit flips at random positions. The probability of a mutation of a bit is $\frac{1}{l}$ where l is the length of the binary vector. Thus, a mutation rate of 1 per mutation and individual selected for mutation is reached.  

    | Genome  |  Position|  Mutated Genome |
    |:-------:|:--------:|:---------------:|
    |  110111 |  1       |      100111     |  

* Flip Bit - This mutation operator takes the chosen genome and inverts the bits (i.e. if the genome bit is 1, it is changed to 0 and vice versa).
  
    |  Genome  | Mutated Genome |
    |:--------:|:--------------:|
    |   110011 |    001100      |

* Boundary - This mutation operator replaces the genome with either lower or upper bound randomly. This can be used for integer and float genes.
    * Lower Bound - 0
    * Upper Bound - 5 

    |   Genome |  Mutated Genome |
    |:--------:|:---------------:|
    |   3      |     0           |
    |   4      |     5           |
    |   1      |      5          |
    |   5      |       0         |

* Scramble - Scramble mutation is also popular with permutation representations. In this, from the entire chromosome, a subset of genes is chosen and their values are scrambled or shuffled randomly. 

    |  Genome  |   Chosen Substring | Mutated Genome |
    |:--------:|:------------------:|:--------------:|
    |  1101101 |        [2,4]       |     1110101    |

# Genetic Algorithm to solve Travelling Salesman Problem

Code for Generic Algorithm with appropriate operators for the problem.

```python
import numpy as np, random, operator, pandas as pd, matplotlib.pyplot as plt
class City:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def distance(self, city):
        xDis = abs(self.x - city.x)
        yDis = abs(self.y - city.y)
        distance = np.sqrt((xDis ** 2) + (yDis ** 2))
        return distance
    
    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"

class Fitness:
    def __init__(self, route):
        self.route = route
        self.distance = 0
        self.fitness= 0.0
    
    def routeDistance(self):
        if self.distance ==0:
            pathDistance = 0
            for i in range(0, len(self.route)):
                fromCity = self.route[i]
                toCity = None
                if i + 1 < len(self.route):
                    toCity = self.route[i + 1]
                else:
                    toCity = self.route[0]
                pathDistance += fromCity.distance(toCity)
            self.distance = pathDistance
        return self.distance
    
    def routeFitness(self):
        if self.fitness == 0:
            self.fitness = 1 / float(self.routeDistance())
        return self.fitness

def createRoute(cityList):
    route = random.sample(cityList, len(cityList))
    return route

def initialPopulation(popSize, cityList):
    population = []

    for i in range(0, popSize):
        population.append(createRoute(cityList))
    return population

def rankRoutes(population):
    fitnessResults = {}
    for i in range(0,len(population)):
        fitnessResults[i] = Fitness(population[i]).routeFitness()
    return sorted(fitnessResults.items(), key = operator.itemgetter(1), reverse = True)

def selection(popRanked, eliteSize):
    selectionResults = []
    df = pd.DataFrame(np.array(popRanked), columns=["Index","Fitness"])
    df['cum_sum'] = df.Fitness.cumsum()
    df['cum_perc'] = 100*df.cum_sum/df.Fitness.sum()
    
    for i in range(0, eliteSize):
        selectionResults.append(popRanked[i][0])
    for i in range(0, len(popRanked) - eliteSize):
        pick = 100*random.random()
        for i in range(0, len(popRanked)):
            if pick <= df.iat[i,3]:
                selectionResults.append(popRanked[i][0])
                break
    return selectionResults

def matingPool(population, selectionResults):
    matingpool = []
    for i in range(0, len(selectionResults)):
        index = selectionResults[i]
        matingpool.append(population[index])
    return matingpool

def breed(parent1, parent2):
    child = []
    childP1 = []
    childP2 = []
    
    geneA = int(random.random() * len(parent1))
    geneB = int(random.random() * len(parent1))
    
    startGene = min(geneA, geneB)
    endGene = max(geneA, geneB)

    for i in range(startGene, endGene):
        childP1.append(parent1[i])
        
    childP2 = [item for item in parent2 if item not in childP1]

    child = childP1 + childP2
    return child

def breedPopulation(matingpool, eliteSize):
    children = []
    length = len(matingpool) - eliteSize
    pool = random.sample(matingpool, len(matingpool))

    for i in range(0,eliteSize):
        children.append(matingpool[i])
    
    for i in range(0, length):
        child = breed(pool[i], pool[len(matingpool)-i-1])
        children.append(child)
    return children

def mutate(individual, mutationRate):
    for swapped in range(len(individual)):
        if(random.random() < mutationRate):
            swapWith = int(random.random() * len(individual))
            
            city1 = individual[swapped]
            city2 = individual[swapWith]
            
            individual[swapped] = city2
            individual[swapWith] = city1
    return individual

def mutatePopulation(population, mutationRate):
    mutatedPop = []
    
    for ind in range(0, len(population)):
        mutatedInd = mutate(population[ind], mutationRate)
        mutatedPop.append(mutatedInd)
    return mutatedPop

def nextGeneration(currentGen, eliteSize, mutationRate):
    popRanked = rankRoutes(currentGen)
    selectionResults = selection(popRanked, eliteSize)
    matingpool = matingPool(currentGen, selectionResults)
    children = breedPopulation(matingpool, eliteSize)
    nextGeneration = mutatePopulation(children, mutationRate)
    return nextGeneration

def geneticAlgorithm(population, popSize, eliteSize, mutationRate, generations):
    pop = initialPopulation(popSize, population)
    progress = []
    progress.append(1 / rankRoutes(pop)[0][1])
    
    for i in range(0, generations):
        pop = nextGeneration(pop, eliteSize, mutationRate)
        progress.append(1 / rankRoutes(pop)[0][1])
    
    plt.plot(progress)
    plt.ylabel('Distance')
    plt.xlabel('Generation')
    plt.show()
```

Initialise a random city list and random distances for the problem

```python
cityList = []

for i in range(0,25):
    cityList.append(City(x=int(random.random() * 200), y=int(random.random() * 200)))

cityList
```

> ```
>   [(89,136), (151,178), (180,161), (49,55), (198,39), (51,122), (60,125), (160,95),                                       (131,184), (192,53), (171,171), (158,125), (168,198), (78,177), (171,64), (47,44), (12,139), (154,152), (187,118), (106,13), (163,88), (110,28), (189,31), (115,114),   (161,25)]
> ```

Call the function

```python
geneticAlgorithm(population=cityList, popSize=100, eliteSize=20, mutationRate=0.01, generations=500)
```

> ![](output_2_0.png)

# Classifiers
## SLIQ
SLIQ is a decision tree classifier that can handle both numeric and categorical attributes. SLIQ uses a pre-sorting technique in the tree-growth phase to reduce the cost of evaluating numeric attributes. This sorting procedure is integrated with a breadth-first tree growing strategy to enable SLIQ to classify disk-resident datasets. In addition, SLIQ uses a fast subsetting algorithm for determining splits for categorical attributes. SLIQ also uses a new tree-pruning algorithm based on the Minimum Description Length principle. This algorithm is inexpensive, and results in compact and accurate trees. The combination of these techniques enables SLIQ to scale for large data sets and classify data sets with a large number of classes, attributes, and examples.

### Pseudocode

1. Start Pre-sorting of the samples.
    * For each attribute, create an attribute list with columns for the value and an index into the class list (sample-ID).
    * Create a class list with columns for the sample-ID, class label and leaf node.
    * Iterate over all training samples, for each attribute:
        * Insert its attribute values, sample-ID and class (sorted by attribute value) into the attributelist.
        * Insert the sample-ID, the class and the leaf node (sorted by sample-ID) into the class list.
2. As long as the stop criterion has not been reached:
    * For every attribute
        * Place all nodes into a class histogram.
        * Start evaluation of the splits.
            * For each node, and for all attributes construct a histogram (for each class the histogram saves the count of samples before and after the split.
            * For each attribute A, for each value v (traverse the attribute list for A)
                * Find the entry in the class list (provides the class and node).
                * Update the histogram for the node.
                * Assess the split.
    * Choose a split.
    * Update the decision tree; for each new node update its class list (nodes).
        * Traverse the attribute list of the attribute used in the node.
        * For each entry (value, ID) find the matching entry (ID, class, node) in the class list.
        * Apply the split criterion emitting a new node.
        * Replace the corresponding class list entry with (ID, class, new node).

### Trace

* Dataset

|  Age | Salary | Class|
|------|--------|------|
|   30 |    65  |   G  |
|   23 |    15  |   B  |
|   40 |    75  |   G  |
|   55 |   40   |   B  |
|   55 |   100  |   G  |
|   45 |    60  |   G  |

* Pre Sorting

    * Age List

        |  Age|  Index|
        |-----|-------|
        |  23 |    2  |
        |  30 |    1  |
        |  40 |    3  |
        |  45 |    6  |
        |  55 |    5  |
        |  55 |    4  |

    * Salary List

        |  Salary|  Index|
        |--------|-------|
        |  15    |    2  |
        |  40    |    4  |
        |  60    |    6  |
        |  65    |    1  |
        |  75    |    3  |
        |  100   |    5  |

    * Class List

        |  Class|  Leaf|
        |-------|------|
        |  G    |   N1 |
        |  B    |   N1 |
        |  G    |   N1 |
        |  B    |   N1 |
        |  G    |   N1 |
        |  G    |   N1 |

* Evaluating Splits
    * Salary List

        |  Salary|  Index|
        |--------|-------|
        |  15    |    2  |
        |  40    |    4  |
        |  60    |    6  |
        |  65    |    1  |
        |  75    |    3  |
        |  100   |    5  |

    * Class List

        |  Class|  Leaf|
        |-------|------|
        |  G    |   N2 |
        |  B    |   N2 |
        |  G    |   N3 |
        |  B    |   N3 |
        |  G    |   N3 |
        |  G    |   N3 |

* Initial histograms
    * N2

        |     |  B  |  G  |
        |-----|-----|-----|
        |  L  |  0  |  0  |
        |  R  |  1  |  1  |

    * N3

        |     |  B  |  G |
        |-----|-----|----|
        |  L  |  0  |  0 |
        |  R  |  1  |  3 |

* Update class histograms and evaluate first split for node N2 (salary less than equal to 15)

    * N2

        |     |  B  |  G  |
        |-----|-----|-----|
        |  L  |  1  |  0  |
        |  R  |  0  |  1  |

    * N3

        |     |  B  |  G |
        |-----|-----|----|
        |  L  |  0  |  0 |
        |  R  |  1  |  3 |

* Update class histograms and evaluate first split for node N3 (salary less than equal to 40)

    * N2

        |     |  B  |  G  |
        |-----|-----|-----|
        |  L  |  1  |  0  |
        |  R  |  0  |  1  |

    * N3

        |     |  B  |  G |
        |-----|-----|----|
        |  L  |  1  |  0 |
        |  R  |  0  |  3 |

## ARBC classifier
Association rule learning is a rule-based machine learning method for discovering interesting relations between variables in large databases. It is intended to identify strong rules discovered in databases using some measures of interestingness. Based on the concept of strong rules, Rakesh Agrawal, Tomasz Imieliński and Arun Swami introduced association rules for discovering regularities between products in large-scale transaction data recorded by point-of-sale (POS) systems in supermarkets. In addition to the above example from market basket analysis association rules are employed today in many application areas including Web usage mining, intrusion detection, continuous production, and bioinformatics. In contrast with sequence mining, association rule learning typically does not consider the order of items either within a transaction or across transactions.

### Pseudocode
1. Generate association rules using an associative rule mining algorithm such as Apriori or FP-Growth.
2. Given two rules, $r_i$ and $r_j$, $r_i$ precedes $r_j$ or $r_i$ has a higher precedence than $r_j$ if
    * the confidence of ri is greater than that of rj, or
    * their confidences are the same, but the support of ri is greater than that of rj, or   
    * both the confidences and supports of riand rj are the same, but ri is generated earlier than rj.

```
R = sort(R);
for each rule r∈R in sequence do   
    if there are still training examples in D AND r classifies   
        at least one example correctly then 
        delete all training examples covered by r from D;
        add r to the classifier  
    end
end 
add the majority class as the default class to the classifier.  
```

### Trace

* Dataset - 

|Sl | Sequence|
|:-:|:-------:|
|1  | abe     |
|2  | ae      |
|3  | be      |
|4  | cdf     |
|5  | cf      |
|6  | df      |
|7  | bce     |
|8  | adf     |

* Rules from Apriori - 

    $\begin{array}{|c|c|c|}
    \hline \text { Rule } & \text { Confidence } & \text { Support } \\
    \hline b \rightarrow e & 1 & 3 \\
    \hline d \rightarrow f & 1 & 3 \\
    \hline b c \rightarrow e & 1 & 1 \\
    \hline a d \rightarrow f & 1 & 1 \\
    \hline c d \rightarrow f & 1 & 1 \\
    \hline a b \rightarrow e & 1 & 1 \\
    \hline a \rightarrow e & 0.66 & 2 \\
    \hline c \rightarrow f & 0.66 & 2 \\
    \hline a \rightarrow f & 0.33 & 1 \\
    \hline c \rightarrow e & 0.33 & 1 \\
    \hline
    \end{array}$

* After performing pruning - 

    $\begin{array}{|c|c|c|c|c|c|c|}
    \hline \text { Rule r } & \mathrm{L} & \text { Rerr } & \mathrm{D} & \text { r.DefaultClass } & \text { Derr } & \text { r.Terr } \\
    \hline b \rightarrow e & {[3,0]} & 0 & {[1,4]} & \mathrm{f} & 1 & 1 \\
    \hline d \rightarrow f & {[0,3]} & 0 & {[1,1]} & \mathrm{e} & 1 & 1 \\
    \hline b c \rightarrow e & {[1,0]} & 0 & {[0,1]} & \mathrm{f} & 0 & 0 \\
    \hline a d \rightarrow f & {[0,1]} & 0 & {[0,0]} & \mathrm{e} & 0 & 0 \\
    \hline
    \end{array}$

* We add the rules to the classifier in the descending order of Terr. The default class is *f*. 

* Final classifier - 

    $b \rightarrow e$ 

    $d \rightarrow f$  

    $b c \rightarrow e$  

    Default f

# Sequence Pattern Mining
## GSP
GSP algorithm (Generalized Sequential Pattern algorithm) is an algorithm used for sequence mining. The algorithms for solving sequence mining problems are mostly based on the apriori (level-wise) algorithm. One way to use the level-wise paradigm is to first discover all the frequent items in a level-wise fashion. It simply means counting the occurrences of all singleton elements in the database. Then, the transactions are filtered by removing the non-frequent items. At the end of this step, each transaction consists of only the frequent elements it originally contained. This modified database becomes an input to the GSP algorithm. This process requires one pass over the whole database.

### Pseudocode
* Initially, every item in DB is a candidate of length-1
* for each level (i.e., sequences of length-k) do
    * scan database to collect support count for each candidate sequence
    * generate candidate length-(k+1) sequences from length-k frequent sequences using Apriori
* repeat until no frequent sequence or no candidate can be found

### Trace
* Database -   
    ![](gsp_dataset.png)
* Sorted Table -   
    ![](gsp_sorted.png)
* We use minimum support as 2 for Apriori -   
    ![](gsp_apriori.png)
* The more obvious one is that item E did not have enough support to meet the threshold, so it get's removed.  Based on the Apriori principle, we are able to conclude that because E is not frequent, then any sequence with E in it would not be frequent either. So, we don't need to keep track of it anymore. The next tricky is thing is that the support for B is listed at 5, not 6, even though the 3rd customer bought item B twice. This practice is also borrowed from the Apriori algorithm.  In the Apriori algorithm, if a customer buys 2 candy bars at once, then we only count 1 candy bar when calculating the support, because we count transactions.  The same applies here, except instead of counting how many transactions contain an item, or itemset, we are counting how many customers have an item/sequence. Once we have these items we start the iterative process of generating larger patterns from these items and checking if they have support. This is where things get really interesting.  In Apriori, the total possible combinations would be AB, AC, AD, AF, AG, BC, BD, BF, BG, CD, CF, CG, DF, DG, FG.  When generating sequences, that is not nearly sufficient because the order matters, AND we can have sequences where single items repeat (e.g. I bought A, then the next day I bought A again). So we have to create tables that look like this -   
    ![](gsp_1.png)
    ![](gsp_2.png) 

    The diagonal values are blank here because they're already covered in the first2-item table.  Just to make sure you understand, (AA) isn't there because if Aand A were bought in the same transaction it would only be counted once so wenever have two of the same item together within a parentheses.  The lower leftis also blank, but this is because of the convention I already described ofalways listing items purchased together in ascending order. 
* We can check our 51 possible 2-item sequences against the database to see if they meet the support threshold.  
    ![](gsp_3.png)
* In the first column, we see that there are no customers that ever bought item A on more than 1 occasion, so support there is 0.  For < AB >, you can see that customers 1 and 5 bought those items in that sequence.  For < AC >, notice that customer 4 bought (AB) together then C, but we get to pick out just A from that combined transaction to get our < AC > sequence. Notice that the items don't have to be right next to each other.  It's OK to have other items in between. < AD > follows similar logic. For < AF > we don't count customers 3 or 4 because although they have A and F, they aren't in the right order. To be counted here, they need to be A, then F.  When we look for the support of < FA > those will get counted. There is one last rule you need to know for counting support of sequences. If you are looking for support of a sequence like < FG >, then customer 1 doesn't count.  It only counts if F is bought before G, NOT at the same time.  If you work through the rest of the 2-item sequences that weren't purchased together (i.e. not ones like (AB)), you get support counts that look like this -   
    ![](gsp_4.png)
* Now let's take a look at the 2-item sets that could have been purchased together (e.g. (AB)).  This one is a little bit easier, because there are only 5 times in our database when 2 items were purchased at the same time.  They were (FG), (AB), (AB), (BC) and (DE).  Only (AB) shows up twice so we keep it and get rid of the rest. Now we have these 2-item sequences left to work with:  
    ![](gsp_5.png)
* You take all of the remaining sequences from the last step (AB, AC, AD, AF, AG, BC...), then you remove the first item from the sequence.  
    ![](gsp_6.png)
* We combine the sequences together where their "-1st" and "-Last" columns match. Working through the rest of the table this way (being careful not to create any duplicate candidates) we populate the "3-seq after join" column in the table below  
    ![](gsp_7.png)
* The next step is to prune these 3-item sequences. This pruning helps save computing time because we don't want to comb through the database to find the support for a sequence that we know won't meet the support threshold.  Now that we have this slimmed down list of 3-item sequences, we scan the database again to determine support just as it was described above for 2-item sequences.  When that is completed we end up with the 3-item sequences in the last column of the table above.   Take that list of 3-item sequences and generate 4-item sequences using the same method.  We remove the first and last items and put them in the "-1st" and "-Last" columns below.  
    ![](gsp_8.png)
* Then we look for sequences that have a "-1st" value that match a "-Last" value of another sequence. In this case we only find matches for "BF" and "BG".  You should prune the sequences if they aren't supported by 3-item sequences that are supported/frequent, but in this case they're fine.  Then we check the database for support to finalize the list.   
    ![](gsp_9.png)
* Output  
    ![](gsp_10.png)

## Prefix Span Algorithm
PrefixSpan (Prefix-projected Sequential pattern mining) algorithm is a very well known algorithm for sequential data mining. It extracts the sequential patterns through a pattern growth method. The algorithm performs very well for small datasets. As the size of datasets increases the overall time for finding the sequential patterns also get increased.

### Pseudocode

![Pseudocode](psa_algo.png)

### Trace
* 1  
    ![](psa_1.png)
* 2  
    ![](psa_2.png)
* 3  
    ![](psa_3.png)
* 4  
    ![](psa_4.png)
* 5  
    ![](psa_5.png)
* 6  
    ![](psa_6.png)

# Schemata Theorem of GA
Holland’s schema theorem, also called the fundamental theorem of genetic algorithms, is an inequality that results from coarse-graining an equation for evolutionary dynamics. The Schema Theorem says that short, low-order schemata with above-average fitness increase exponentially in frequency in successive generations. The theorem was proposed by John Holland in the 1970s. It was initially widely taken to be the foundation for explanations of the power of genetic algorithms. However, this interpretation of its implications has been criticized in several publications reviewed in, where the Schema Theorem is shown to be a special case of the Price equation with the schema indicator function as the macroscopic measurement. A schema is a template that identifies a subset of strings with similarities at certain string positions. Schemata are a special case of cylinder sets, and hence form a topological space. Consider binary strings of length 6. The schema 1*10*1 describes the set of all strings of length 6 with 1’s at positions 1, 3 and 6 and a 0 at position 4. The * is a wildcard symbol, which means that positions 2 and 5 can have a value of either 1 or 0. The order of a schema o(H) is defined as the number of fixed positions in the template, while the defining length $\delta$(H) is the distance between the first and last specific positions. The order of 1*10*1 is 4 and its defining length is 5. The fitness of a schema is the average fitness of all strings matching the schema. The fitness of a string is a measure of the value of the encoded problem solution, as computed by a problem-specific evaluation function. Using the established methods and genetic operators of genetic algorithms, the schema theorem states that short, low-order schemata with above-average fitness increase exponentially in successive generations. Expressed as an equation:

\begin{equation}\mathbf{E}(m(H, t+1)) \geq \frac{m(H, t) f(H)}{a_{t}}|1-p|\end{equation}

Here m(H,t) is the number of strings belonging to schema H at generation t, f(H) is the observed average fitness of schema H and a\textsubscript{t} is the observed average fitness at generation t. The probability of disruption p is the probability that crossover or mutation will destroy the schema H. It can be expressed as:

\begin{equation}p=\frac{\delta(H)}{l-1} p_{e}+o(H) p_{m}\end{equation}

here o(H) is the order of the schema, l is the length of the code, $p_m$ is the probability of mutation and $p_c$ is the probability of crossover. So a schema with a shorter defining length $\delta$(H) is less likely to be disrupted. An often misunderstood point is why the Schema Theorem is an inequality rather than an equality. The answer is in fact simple: the Theorem neglects the small, yet non-zero, probability that a string belonging to the schema H will be created ”from scratch” by mutation of a single string (or recombination of two strings) that did not belong to H in the previous generation.

## Trace

* String Processing

|Sl | Population| x  |  f(x) |  Probability |Expected Count |  Actual count|
|:-:|:---------:|:--:|:-----:|:------------:|:-------------:|:------------:|
| 1 | 01101     | 13 | 1872  | 0.06         | 0.24          |0             |
| 2 | 11110     | 30 | 25230 | 0.8          | 3.17          |3             |
| 3 | 01000     | 8  | 392   | 0.013        | 0.05          |0             |
| 4 | 10001     | 17 | 4352  | 0.127        | 0.54          |1             |

\begin{equation}\begin{array}{l}
\sum f(x)=31846 \\
\operatorname{avg} f(x)=7961.5 \\
\max f(x)=25230
\end{array}\end{equation}

* Processing before reproduction

|Sl  | Schema    |  String Representative |Schema Average Fitness |
|:--:|:---------:|:----------------------:|:---------------------:|
| H1 | 1****     | 2,4                    | 14791                 |
| H2 | *10**     | 3                      |  392                  |
| H3 | 1***0     | 2                      |  25230                |

\begin{equation}\begin{array}{l}
m(H 1)=\frac{(4 * 14791)}{7961,5}=7.43 \\
m(H 2)=\frac{(0 * 392)}{7961.5}=0 \\
m(H 3)=\frac{(3 * 25230)}{7961.5}=9.5
\end{array}\end{equation}

* Crossover

|Mating Pool| Mate  |  Crossover Site |New Population|x Value |  f(x)   |
|:---------:|:-----:|:---------------:|:------------:|:------:|:-------:|
| 01101     |  2    |      2          |  11110       | 30     |   25230 |
| 11110     | 1     |      2          |  11110       | 30     |   25230 |
| 01000     | 4     |      2          |  11101       | 29     |   22736 |
| 10001     | 3     |      2          |  10010       | 18     |   5202  |

\begin{equation}\begin{array}{l}
\sum f(x)=78398 \\
\operatorname{avg} f(x)=19599.5 \\
\max f(x)=25230
\end{array}\end{equation}

* Schema Processing
For the element 11110, if mutation is performed in the bit 0, 11111 is obtained which is 31 which gives a fitness value of 27900.

**After all Operations**

|Expected count|Actual count| String representatives|
|:------------:|:----------:|:---------------------:|       
|7.43          |4           |  2,4                  |
|0             |0           |  3                    |
|9.5           |3           |  2                    |  

# Regression Models
## Linear Regression
Linear regression is a statistical approach which quantifies the relationship between one or more predictor (independent) variable(s) and one outcome (dependent) variable. Linear regression is commonly used for predictive analysis and modelling.

Given a data set $\left\{y_{i}, x_{i 1}, \ldots, x_{i p}\right\}_{i=1}^{n}$ of n statistical units, a linear regression model assumes that the relationship between the dependent variable y and the p-vector of regressors x is linear. This relationship is modelled through a disturbance term or error variable $\epsilon$ that adds ”noise” to the linear relationship between the dependent variable and regressors. Thus the model takes the form:

\begin{equation}y_{i}=\beta_{0}+\beta_{1} x_{i 1}+\ldots+\beta_{p} x_{i p}+\epsilon_{i}, \mathrm{i}=1, \ldots, \mathrm{n}\end{equation}

Linear regression with a single predictor variable is known as simple regression. In real-world applications, there is typically more than one predictor variable. Such regressions are called multiple regression.

### Pseudocode
* Calculate $\sum_{i=1}^{n} X_{i}, \sum_{i=1}^{n} X_{i}^{2}, \sum_{i=1}^{n} Y_{i} \sum_{i=1}^{n} X_{i}Y_{i}$
* Calculate a and b in y=ax+b as follows -

\begin{equation}a=\frac{n * \sum_{i=1}^{n} X_{i} Y_{i}-\sum_{i=1}^{n} X_{i} * \sum_{i=1}^{n} Y_{i}}{n * \sum_{i=1}^{n} X_{i}^{2}-\sum_{i=1}^{n} X_{i} \sum_{i=1}^{n} X_{i}}, b=\frac{\sum_{i=1}^{n} Y_{i}-a * \sum_{i=1}^{n} X_{i}}{n}\end{equation}

### Trace

* Dataset

|Sl   | Monthly Sales (in 1000)|Online Advertising revenue(in millions)|
|:---:|:----------------------:|:-------------------------------------:| 
|1    | 368                    |     1.7                               |
|2    | 340                    |     1.5                               |
|3    | 665                    |     2.8                               |
|4    | 954                    |     5                                 |
|5    | 331                    |     1.3                               |
|6    | 556                    |     2.2                               |
|7    | 376                    |     1.3                               |

* we find a and b using the above formulae.
    * a = 171.5
    * b = 125.8

* The data fits the curve $y=171.5*x+125.8$  
    ![](linear.png)

## Non-Linear Regression
In statistics, nonlinear regression is a form of regression analysis in which observational data are modeled by a function which is a nonlinear combination of the model parameters and depends on one or more independent variables. The data are fitted by a method of successive approximations.

### Pseudocode
1. Initialise variables.
2. Evaluate the parameters based on the assumed function.
3. Evaluate convergence criterion.
4. Exit if the criterion < tolerance or if max iterations is reached.

### Trace
* Used equation:

\begin{equation}y_{i}=\beta_{0}+\beta_{1} \exp \left(\beta_{2} x_{i, 1}+\ldots+\beta_{k+1} x_{i, k}\right)+\epsilon_{i}\end{equation}

where the $\epsilon_i$ are iid normal with mean 0 and constant variance $\sigma_2$. Notice that if $\beta_0=0$, then the above is intrinsically linear by taking the natural logarithm of both sides.  
* Dataset  
    ![](nlr_data.png)  
* The response variable, Y, is the prognostic index for long-term recovery and the predictor variable, X, is the number of days of hospitalization. The proposed model is the two-parameter exponential model:
\begin{equation}Y_{i}=\theta_{0} \exp \left(\theta_{1} X_{i}\right)+\epsilon_{i}\end{equation}  
* Statistical software nonlinear regression routines are available to apply the Gauss-Newton algorithm to estimate $\theta_0$ and $\theta_1$. Before we do this, however, we have to find initial values for $\theta_0$ and $\theta_1$. One way to do this is to note that we can linearize the response function by taking the natural logarithm:  

\begin{equation}\log \left(\theta_{0} \exp \left(\theta_{1} X_{i}\right)\right)=\log \left(\theta_{0}\right)+\theta_{1} X_{i}\end{equation}

* Thus we can fit a simple linear regression model with response, $\log (Y)$, and predictor, $X$, and the intercept (4.0372) gives us an estimate of $\log \left(\theta_{0}\right)$ while the slope (-0.03797) gives us an estimate of $\theta_{1}$. (We then calculate $\exp (4.0372)=56.7$ to estimate $\theta_{0}$.  
* Now we can fit the nonlinear regression model. The following output was obtained using software -   
    
> ```
> Nonlinear Regression: prog = Theta1 * ex (Theta2 * days)
> 
> Method
> Algorithm        Gauss-Newton
> Max iterations            200
> Tolerance             0.00001
> 
> Starting Values for Parameters
> Parameter   Value
> Theta1       56.7
> Theta2     -0.038
> 
> Equation
> prog = 58.6066 * exp(-0.0395865 * days)
> 
> Parameter Estimates
> Parameter  Estimate  SE Estimate
> Theta1      58.6066      1.47216
> Theta2      -0.0396      0.00171
> 
> prog = Theta1 * exp(Theta2 * days)
> 
> Summary
> Iterations        5
> Final SSE   49.4593
> DFE              13
> MSE         3.80456
> S           1.95053
> ```

* Fitted Curve  
    ![](nlr_out.png)
