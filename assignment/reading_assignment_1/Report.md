---
title: "Reading Assignment - 1"
author: [S Pawan Kumar, CED16I043]
date: "2020-04-26"
titlepage: true
table-use-row-colors: true
classoption: [oneside]
...

# 1
## a) Decision Tree Induction
### Pseudo Code

```
Input:

    Data partition, D, which is a set of training tuples and their associated class labels.
    attribute_list, the set of candidate attributes.
    Attribute selection method, a procedure to determine 
        the splitting criterion that best partitions that the data 
        tuples into individual classes. This criterion includes a 
        splitting_attribute and either a splitting point or splitting subset.

Method:
create a node N;

if tuples in D are all of the same class, C then
   return N as leaf node labeled with class C;
   
if attribute_list is empty then
   return N as leaf node with labeled 
   with majority class in D;|| majority voting
   
apply attribute_selection_method(D, attribute_list) 
to find the best splitting_criterion;
label node N with splitting_criterion;

if splitting_attribute is discrete-valued and
   multiway splits allowed then  // no restricted to binary trees

attribute_list = splitting attribute; // remove splitting attribute
for each outcome j of splitting criterion

   // partition the tuples and grow subtrees for each partition
   let Dj be the set of data tuples in D satisfying outcome j; // a partition
   
   if Dj is empty then
      attach a leaf labeled with the majority 
      class in D to node N;
   else 
      attach the node returned by Generate 
      decision tree(Dj, attribute list) to node N;
   end for
return N;

```

### Measurement Parameters

* Information Gain
    
    Information is defined as - 
        
    \begin{equation}\operatorname{Info}(D)=-\sum_{i=1}^{m} p_{i} \log _{2}\left(p_{i}\right)\end{equation}

    where _pi_ is the non-zero probability that the sample belongs to class i.

    \begin{equation}\operatorname{Info}_{A}(D)=\sum_{j=1}^{v} \frac{\left|D_{j}\right|}{|D|} \times \operatorname{Info}\left(D_{j}\right)\end{equation}

    The term $\frac{\left|D_{j}\right|}{|D|}$ acts as weights of the _jth_ partition.

    Information gain is defined as the difference between the original information
requirement (i.e., based on just the proportion of classes) and the new requirement (i.e.,
obtained after partitioning on A).

    \begin{equation}\operatorname{Gain}(A)=\operatorname{Info}(D)-\operatorname{Info}_{A}(D)\end{equation}

    The attribute with _maximum_ **Information Gain** is selected.
 
* Gain Ratio

    Split Information is defined analogous to information as - 

    $\operatorname{SplitInfo}_{A}(D)=-\sum_{j=1}^{v} \frac{\left|D_{j}\right|}{|D|} \times \log _{2}\left(\frac{\left|D_{j}\right|}{|D|}\right)$

    The gain ration is hence defined as - 

    \begin{equation}\operatorname{GainRatio(A)}=\frac{\operatorname{Gain}(A)}{\text {SplitInfo}_{A}(D)}\end{equation}

    The attribute with _maximum_ **Gain Ratio** is selected.


### Trace

**Dataset Used**  

   | Day | Outlook  | Temperature| Humidity | Wind  | PlayTennis|
   |-----|----------|------------|----------|-------|-----------|
   | D1  |  Sunny   |      Hot   |    High  | Weak  | No        |
   | D2  |   Sunny  |   Hot      |    High  | Strong| No        |
   | D3  | Overcast |    Hot     |    High  | Weak  | Yes       |
   | D4  | Rain     |   Mild     |    High  | Weak  |  Yes      |  
   | D5  | Rain     |   Cool     |    Normal| Weak  |     Yes   |
   | D6  | Rain     |   Cool     |  Normal  | Strong|      No   |
   | D7  | Overcast |   Cool     |  Normal  | Strong|      Yes  |
   | D8  | Sunny    |   Mild     |  High    | Weak  |       No  |
   | D9  | Sunny    |   Cool     |  Normal  | Weak  |        Yes|
   | D10 | Rain     |   Mild     |  Normal  | Weak  |     Yes   |
   | D11 | Sunny    |   Mild     |  Normal  | Strong|     Yes   |
   | D12 |  Overcast|   Mild     |    High  | Strong|     Yes   |
   | D13 |  Overcast|   Hot      |   Normal | Weak  |     Yes   |
   | D14 |  Rain    |   Mild     |    High  | Strong|      No   |

* C\textsubscript{1} = yes
* C\textsubscript{2} = no

We consider Information Gain to be the selection criteria here.

1.
    * Entropy of the dataset 
    \begin{equation}\mathrm{Info}(\mathrm{D})=-\left(\frac{9}{14} \log_{2} \frac{9}{14}+\frac{5}{14} \log_{2} \frac{5}{14}\right)=0.940\end{equation}
    * Information gain for each attribute
        * $\operatorname{Gain}(Humidity)=0.940-\left(\frac{7}{14} * -\left(\frac{3}{7} \log_{2} \frac{3}{7}+\frac{4}{7} \log_{2} \frac{4}{7}\right)+\frac{7}{14} * -\left(\frac{6}{7} \log_{2} \frac{6}{7}+\frac{1}{7} \log_{2} \frac{1}{7}\right)\right)=0.151$ 
        * $\operatorname{Gain}(Wind)=0.048$
        * $\operatorname{Gain}(Outlook)=0.246$
        * $\operatorname{Gain}(Temperature)=0.246$
    * We choose Outlook to branch because of high Gain value.

2. When we take Overcast branch for Outlook we directly arrive at the class label C\textsubscript{1}.

3. When we take Sunny branch for Outlook - 
    * Information Gain for various attributes 
        * $\operatorname{Gain}(Wind)=0.020$
        * $\operatorname{Gain}(Humidity)=0.971$
        * $\operatorname{Gain}(Temperature)=0.571$ 
    * We choose Humidity to branch over.
    * We reach the Class labels on branching over Humidity.

4. When we take Rain branch for Outlook -  
    * Information Gain for various attributes 
        * $\operatorname{Gain}(Wind)=0.97$
        * $\operatorname{Gain}(Humidity)=0.20$
        * $\operatorname{Gain}(Temperature)=0.20$ 
    * We choose Wind to branch over.
    * We reach the Class labels on branching over Wind.

![Decision Tree](decisiontree.png)

## b) Naive Bayes Classifier
### Bayes Theorem

In Bayesian terms, X is considered “evidence.” As usual, it is described by
measurements made on a set of n attributes. Let H be some hypothesis such as that
the data tuple X belongs to a specified class C. For classification problems, we want to
determine P(H|X), the probability that the hypothesis H holds given the “evidence” or
observed data tuple X. In other words, we are looking for the probability that tuple X
belongs to class C, given that we know the attribute description of X.

P(H|X) is the posterior, or probability of H conditioned on X. P(H) is the prior probability, of H. P(X|H) is the likelihood, or probability of X conditioned on H. P(X) is the marginal, or probability of X. P(H), P(X|H), and P(X) may be estimated from the given data. Hence P(H|X) can be calculated as follows - 

\begin{equation}P(H | X)=\frac{P(X | H) P(H)}{P(X)}\end{equation}

Every attribute is considered to be independent and hence using the following formula we calculate the probability that a sample belongs to a particular class - 

\begin{equation}P\left(\mathbf{X} | C_{i}\right) \approx \prod_{k=1}^{n} P\left(x_{k} | C_{i}\right)\end{equation}


### Conditional independence

A and B are conditionally independent given C if and only if, given knowledge that C occurs, knowledge of whether A occurs provides no information on the likelihood of B occurring, and knowledge of whether B occurs provides no information on the likelihood of A occurring i.e they are mutually independent.

When A and B are two mutually independent events - 

\begin{equation}P(A \cap B)=P(A) \cdot P(B)\end{equation}

Hence the following also holds true - 

\begin{equation}P(A \cap B | C)=P(A | C) \cdot P(B | C)\end{equation}

### Laplacian estimator for zero probability scenario

We can assume thatour training set is so large that adding one to each count that we needwould only make a negligible difference in the estimated probabilities,yet would avoid the case of zero probability values. This techniqueis know as Laplacian correction (or Laplace estimator). If we haveqcounts to which we each add one, then we must remember to addqtothe corresponding denominator used in the probability calculation.

In the following problem we assume q=1.

### Pseudocode

- Learning Phase: Given a training set S.
For each targd value of $c_{i}\left(c_{i}=c_{1}, \cdots, c_{L}\right)$ $\hat{P}\left(C=c_{i}\right) \leftarrow$ estimate $P\left(C=c_{i}\right)$ with examples in $\mathbf{S}$ For every feature value $x_{j k}$ of each feature $X_{j}\left(j=1, \cdots, n ; k=1, \cdots, N_{j}\right)$ $\hat{P}\left(X_{j}=x_{j k} | C=c_{i}\right) \leftarrow$ estimate $P\left(X_{j}=x_{j k} | C=c_{i}\right)$ with examples in $\mathbf{S}$

    Output: conditional probability tables; for $X_{j}, N_{j} \times L$ elements

- Test Phase: Given an unknown instance $\quad \mathbf{X}^{\prime}=\left(a_{1}^{\prime}, \cdots, a_{n}^{\prime}\right)$
Look up tables to assign the label $c^{\star}$ to $\mathbf{X}$ ' if 
$\left[\hat{P}\left(a_{1}^{\prime} | c^{*}\right) \cdots \hat{P}\left(a_{n}^{\prime} | c^{*}\right)\right] \hat{P}\left(c^{*}\right)>\left[\hat{P}\left(a_{1}^{\prime} | c\right) \cdots \hat{P}\left(a_{n}^{\prime} | c\right)\right] \hat{P}(c), \quad c \neq c^{*}, c=c_{1}, \cdots, c_{L}$

### Trace

**Dataset used**

   | Day | Outlook  | Temperature| Humidity | Wind  | PlayTennis|
   |-----|----------|------------|----------|-------|-----------|
   | D1  |  Sunny   |      Hot   |    High  | Weak  | No        |
   | D2  |   Sunny  |   Hot      |    High  | Strong| No        |
   | D3  | Overcast |    Hot     |    High  | Weak  | Yes       |
   | D4  | Rain     |   Mild     |    High  | Weak  |  Yes      |  
   | D5  | Rain     |   Cool     |    Normal| Weak  |     Yes   |
   | D6  | Rain     |   Cool     |  Normal  | Strong|      No   |
   | D7  | Overcast |   Cool     |  Normal  | Strong|      Yes  |
   | D8  | Sunny    |   Mild     |  High    | Weak  |       No  |
   | D9  | Sunny    |   Cool     |  Normal  | Weak  |        Yes|
   | D10 | Rain     |   Mild     |  Normal  | Weak  |     Yes   |
   | D11 | Sunny    |   Mild     |  Normal  | Strong|     Yes   |
   | D12 |  Overcast|   Mild     |    High  | Strong|     Yes   |
   | D13 |  Overcast|   Hot      |   Normal | Weak  |     Yes   |
   | D14 |  Rain    |   Mild     |    High  | Strong|      No   |

The dataset contains 2 classses.  
C\textsubscript{1} - Yes  
C\textsubscript{2} - No  

**Learning Phase**  

\begin{longtable}[1]{lllll}
\caption[Table for Outlook]{Likelihood Table for Outlook} \\
\toprule
Outlook & Frequency(Yes) & Frequency(No) & Likelihood(Yes) & Likelihood(No)\tabularnewline
\midrule
\endhead
Sunny & 2 & 3 & 2/9 & 4/8 \tabularnewline
Overcast & 4 & 0 & 4/9 & 1/8 \tabularnewline
Rainy & 3 & 2 & 3/9 & 3/8 \tabularnewline
\bottomrule
\end{longtable}

\begin{longtable}[2]{lllll}
\caption[Table for Temperature]{Likelihood Table for Temperature} \\
\toprule
Temperature & Frequency(Yes) & Frequency(No) & Likelihood(Yes) & Likelihood(No)\tabularnewline
\midrule
\endhead
Hot & 2 & 2 & 2/9 & 2/5 \tabularnewline
Mild & 4 & 2 & 4/9 & 2/5 \tabularnewline
Cold & 3 & 1 & 3/9 & 1/5 \tabularnewline
\bottomrule
\end{longtable}

\begin{longtable}[3]{lllll}
\caption[Table for Humidity]{Likelihood Table for Humidity} \\
\toprule
Humidity & Frequency(Yes) & Frequency(No) & Likelihood(Yes) & Likelihood(No)\tabularnewline
\midrule
\endhead
High & 3 & 4 & 3/9 & 4/5 \tabularnewline
Normal & 6 & 1 & 6/9 & 1/5 \tabularnewline
\bottomrule
\end{longtable}

\begin{longtable}[4]{lllll}
\caption[Table for Wind]{Likelihood Table for Wind} \\
\toprule
Wind & Frequency(Yes) & Frequency(No) & Likelihood(Yes) & Likelihood(No)\tabularnewline
\midrule
\endhead
Strong & 3 & 3 & 3/9 & 3/5 \tabularnewline
Weak & 6 & 2 & 6/9 & 2/5 \tabularnewline
\bottomrule
\end{longtable}

**Test Phase**  
* Test entry -  
    _X = (Outlook = Sunny, Temperature = Cool, Humidity = High, Wind = Strong)_  

* Lookup from the tables -  
    * P(Outlook=Sunny | Play=Yes) = 2/9  
    * P(Outlook=Sunny| Play=No) = 4/8  
    * P(Temperature=Cool | Play=Yes) = 3/9  
    * P(Temperature=Cool| Play=No) = 1/5  
    * P(Humidity=High | Play=Yes) = 3/9  
    * P(Humidity=High  | Play=No) = 4/5  
    * P(Wind=Strong | Play=Yes) = 3/9  
    * P(Wind=Strong | Play=No) = 3/5  
    * P(Play=Yes) = 9/14  
    * P(Play=No) = 5/14  

* P(Yes|X) ≈ P(X | Yes)P(Yes)
≈ [P(Sunny|Yes)P(Cool|Yes)P(High|Yes)P(Strong|Yes)]P(Yes) = 0.0053

* P(No|X) ≈ P(X | Yes)P(Yes)
≈ [P(Sunny|No) P(Cool|No)P(High|No)P(Strong|No)]P(No) = 0.0171

* Given the fact P(Yes|X) < P(No|X), we label X to be C\textsubscript{2}. Hence the given record is classified as _No_.

## c) Neural Network Classifier (Back Propagation Network)
### Terminologies
* Topology: The way in which the neurons(basic units of a neural network) are connected together determines the topology of the neural network.
* Input layer: The first layer of a neural net, that accepts certain input patterns and generates output values to the succeeding layers for further processing.
* Hidden layer: The layer/layers that lie between a network's input and output layers. It is called hidden, because its neuron values are not visible outside the net. The neurons in this layer apply weights to the inputs and direct them through an activation function as the output for the subsequent layer. The usage of hidden layers extends a neural net’s abilities to learn logical operations.
* Output layer: The last layer of a neural network, that produces the output value(class label) of the network.
* Activation function: A mathematical function that a neuron uses to produce an output. Usually this input value has to exceed a specified threshold value that determines if an output to other neurons should be generated. Functions such as Sigmoid are often used.
* Bias: A hyperparameter of a neural network which improves the accuracy.
* Weight: The strength of a connection between two neurons. It is used to scale the inputs. This is the value which is adjusted during the training phase.

### Strengths

* Ability to handle arbritarily large datasets.
* Application in a variety of fields including speech, image recognition, image captioning, natural language processing, handwriting recognition, etc.
* A trained neural network can be reused on similar problems/subproblems to the problem it was originally trained for.
* Corruption of one or more cells of ANN does not prevent it from generating output.
* Ability to work with incomplete knowledge.
* Artificial neural networks learn events and make decisions by commenting on similar events. Hence they can be used to make machines learn.
* A network slows over time and undergoes relative degradation.
* Artificial neural networks have numerical strength that can perform more than one job at the same time.

### Limitations

* Artificial neural networks(ANN) require processors with parallel processing power, by their structure. For this reason, the realization of the equipment is dependent.
Unexplained functioning of the network: This is the most important problem of ANN.
* When ANN gives a probing solution, it does not give a clue as to why and how. This
reduces trust in the network.
* There is no specific rule for determining the structure of artificial neural networks. The appropriate network structure is achieved through experience, trial and error.
* ANNs can work with numerical information only. Problems have to be translated into numerical values before being introduced to ANN. The display mechanism to be determined here will directly influence the performance of the network. This depends on the user's ability.
* The network is reduced to a certain value of the error on the sample means that the training has been completed. This value does not give us optimum results.
* They are very computationally expensive to train. It is only the advancements in GPUs that has made training neural networks viable.

### And Gate 

* Network -

![Network for And gate](and.png)

* Evaluation equation (b=1 generally) - 
\begin{equation} y=x_{1}+x_{2}-1 \end{equation}

* The activation function -

\begin{equation}f=\left\{\begin{array}{ll}
0, & \text { if } \sum_{i=1}^{n} w_{i} x_{i}+b \leq 0 \\
1, & \text { if } \sum_{i=1}^{n} w_{i} x_{i}++b>0
\end{array}\right.\end{equation}

* Truth Table (Expected Output) - 

|  x\textsubscript{1} | x\textsubscript{1} |  y\textsubscript{actual}|
|:--------------:|:-------------:|:------------------:|
|         0      |       0       |   0                |
|         0      |       1       |   0                |
|         1      |       0       |   0                |
|         1      |       1       |   1                |

* Substituting in the equation we get (actual output) - 

|  x\textsubscript{1} | x\textsubscript{1} | Equation    | Output(y\textsubscript{pred})|
|:--------------:|:-------------:|:-----------:|:-----------------------:|
|         0      |       0       |   $0+0-1=-1$|          0              |
|         0      |       1       |   $0+1-1=0$ |          0              |
|         1      |       0       |   $1+0-1=0$ |           0             |
|         1      |       1       |    $1+1-1=1$|            1            |


### Or Gate 

* Network -

![Network for Or gate](or.png)

* Evaluation equation (b=1 generally) - 
\begin{equation} y=2x_{1}+2x_{2}-1 \end{equation}

* The activation function -

\begin{equation}f=\left\{\begin{array}{ll}
0, & \text { if } \sum_{i=1}^{n} w_{i} x_{i}+b \leq 0 \\
1, & \text { if } \sum_{i=1}^{n} w_{i} x_{i}++b>0
\end{array}\right.\end{equation}

* Truth Table (Expected Output) - 

|  x\textsubscript{1} | x\textsubscript{1} |  y\textsubscript{actual}|
|:--------------:|:-------------:|:------------------:|
|         0      |       0       |   0                |
|         0      |       1       |   1                |
|         1      |       0       |   1                |
|         1      |       1       |   1                |

* Substituting in the equation we get (actual output) - 

|  x\textsubscript{1} | x\textsubscript{1} | Equation        | Output(y\textsubscript{pred})|
|:--------------:|:-------------:|:---------------:|:-----------------------:|
|         0      |       0       |   $2*0+2*0-1=-1$|          0              |
|         0      |       1       |   $2*0+2*1-1=1$ |          1              |
|         1      |       0       |   $2*1+2*0-1=1$ |           1             |
|         1      |       1       |    $2*1+2*1-1=3$|            1            |


### Backpropagation Algorithm Trace

Note - Here updates to the network are assumed to be performed per input sample during the training period. However in practice, updates to the network are performed for a batch of samples. The process is similar except a minor change to the error function, where the average of the errors for each individual is used in the calculation.

![Example Network for Back Propagation](bpn.png)

**Important equations**

* Activation function used 
    \begin{equation}O_{j}=\frac{1}{1+e^{-I_{j}}}\end{equation}
* At a unit *j* in the hidden or output layer, the input I\textsubscript{j} is given by 
    \begin{equation}I_{j}=\sum_{i} w_{i j} O_{i}+\theta_{j}\end{equation}
* For a unit *j* in the output layer, the error Err j is computed by
    \begin{equation}E r r_{j}=O_{j}\left(1-O_{j}\right)\left(T_{j}-O_{j}\right)\end{equation}
    where O\textsubscript{j} is the actual output of unit *j*, and T\textsubscript{j} is the known target value of the given training tuple.
* The error of a hidden layer unit *j* is
    \begin{equation}E r r_{j}=O_{j}\left(1-O_{j}\right) \sum_{k} E r r_{k} w_{j k}\end{equation}
* Weight update equation is given by
    \begin{equation}\begin{aligned}&\Delta w_{i j}=(l) E r r_{j} O_{i}\\&w_{i j}=w_{i j}+\Delta w_{i j}\end{aligned}\end{equation}
    where *l* is the learining rate(a parameter which controls how fast the network learns)

**Training epoch**

* Sample $X = (1,0,1)$
* Target Label = 1
* l = 0.9
* Initial weights  

|  *w*\textsubscript{14}| *w*\textsubscript{15}|  *w*\textsubscript{24}|  *w*\textsubscript{25}|  *w*\textsubscript{34}| *w*\textsubscript{35}|  *w*\textsubscript{46}| *w*\textsubscript{56}|  $\theta_{4}$|  $\theta_{5}$|  $\theta_{6}$|
|:----------------:|:---------------:|:----------------:|:----------------:|:----------------:|:---------------:|:----------------:|:---------------:|:------------:|:------------:|:------------:|
|          0.2     |     -0.3        |      0.4         |     0.1          | -0.5             |      0.2        |     -0.3         |     -0.2        |    -0.4      |  0.2         |    0.1       |


**Feed Forward Step**
Net Input and Output Calculations

|  Unit, *j*|  *I*\textsubscript{j}                       |  *O*\textsubscript{j}                 |
|-----------|----------------------------------------|----------------------------------|
|    4      | $1*0.2+0*0.4-1*0.5-0.4=-0.7$           |      $1/(1 + e^{0.7} ) = 0.332$  |
|     5     | $−0.3*1 - 0*0.5 + 1*0.2 + 0.2 = 0.1$   |      $1/(1 + e^{−0.1} ) = 0.525$ |
|      6    | $−0.3*0.332 − 0.2*0.525 + 0.1 = −0.105$|      $1/(1 + e^{0.105} ) = 0.474$|

**Error Calculation**

|  Unit, *j*|   *E*\textsubscript{j}                                  |
|-----------|----------------------------------------------------|
|     6     |      $(0.474)(1 − 0.474)(1 − 0.474) = 0.1311$      |
|     5     |       $(0.525)(1 − 0.525)(0.1311)(−0.2) = −0.0065$ |
|     4     |        $(0.332)(1 − 0.332)(0.1311)(−0.3) = −0.0087$|

**Weights and Bias update**

|  Weight        |  New Value                       |
|:--------------:|----------------------------------|
|*w*\textsubscript{46}|$−0.3 + 0.9*0.1311*0.332 = −0.261$|
|*w*\textsubscript{56}|$−0.2 + 0.9*0.1311*0.525 = −0.138$|
|*w*\textsubscript{14}|$0.2 + 0.9*−0.0087*1 = 0.192$     |
|*w*\textsubscript{15}|$−0.3 + 0.9*−0.0065*1 = −0.306$   |
|*w*\textsubscript{24}|$0.4 + 0.9*−0.0087*0 = 0.4$       |
|*w*\textsubscript{25}|$0.1 + 0.9*−0.0065*0 = 0.1$       |
|*w*\textsubscript{34}|$−0.5 + 0.9*−0.0087*1 = −0.508$   |
|*w*\textsubscript{35}|$0.2 + 0.9*−0.0065*1 = 0.194$     |
|$\theta_{4}$    |$0.1 + 0.9*0.1311 = 0.218$        |
|$\theta_{5}$    |$0.2 + 0.9*−0.0065 = 0.194$       |
|$\theta_{6}$    |$−0.4 + 0.9*−0.0087 = −0.408$     |

## d) Nearest Neighbour Classifier

### Algorithm 
In pattern recognition, the k-nearest neighbors algorithm (k-NN) is a non-parametric method used for classification and regression.[1] In both cases, the input consists of the k closest training examples in the feature space. The output depends on whether k-NN is used for classification or regression:

* In k-NN classification, the output is a class membership. An object is classified by a plurality vote of its neighbors, with the object being assigned to the class most common among its k nearest neighbors (k is a positive integer, typically small). If k = 1, then the object is simply assigned to the class of that single nearest neighbor.

* In k-NN regression, the output is the property value for the object. This value is the average of the values of k nearest neighbors.

### Pseudocode

```

Input: Training data samples. Let p be an unknown point.
Output: Class label of point p.

1. Initialize k to your chosen number of neighbors
2. For each data point x i in the training sample, calculate the distance (using any suitable distance
measure) of the point p from that point x i denoted as d(x i , p).
3. Sort the training points in increasing order of distance.
4. Select the first k points from this list.
5. Return the class label which occurs the maximum times among these k points.

```

### Trace

**Dataset Used**

|   Age | Has Disease |
|:-----:|:-----------:|
|  22   |    yes      |
|   23  |      no     |
|   21  |      yes    |
|   18  |      no     |
|   19  |      no     |
|   25  |     yes     |
|   27  |       yes   |
|   29  |       yes   |
|    31 |       no    |
|    45 |       yes   |

* k = 5 
* Consider data point p = (33,-)
* Distance from p

|   Age | Has Disease |   Distance|
|:-----:|:-----------:|:---------:|
|  22   |    yes      |     11    |
|   23  |      no     |      10   |
|   21  |      yes    |       12  |
|   18  |      no     |       15  |
|   19  |      no     |       14  |
|   25  |     yes     |       8   |
|   27  |       yes   |       6   |
|   29  |       yes   |       4   |
|    31 |       no    |       2   |
|    45 |       yes   |       12  |

* The 5 nearest neighbours are - 

|   Age | Has Disease |   Distance|
|:-----:|:-----------:|:---------:|
|   23  |      no     |      10   |
|   25  |     yes     |       8   |
|   27  |       yes   |       6   |
|   29  |       yes   |       4   |
|    31 |       no    |       2   |

* The class count is 
    * yes = 3
    * no = 2

Hence the record is classified into the *Yes* class.

# 2. Genetic Algorithm
Nature has always been a great source of inspiration to all mankind. Genetic Algorithms (GAs) are search based algorithms based on the concepts of natural selection and genetics. GAs are a subset of a much larger branch of computation known as Evolutionary Computation.

In GAs, we have a pool or a population of possible solutions to the given problem. These solutions then undergo recombination and mutation (like in natural genetics), producing new children, and the process is repeated over various generations. Each individual (or candidate solution) is assigned a fitness value (based on its objective function value) and the fitter individuals are given a higher chance to mate and yield more “fitter” individuals. This is in line with the Darwinian Theory of “Survival of the Fittest”.

In this way we keep “evolving” better individuals or solutions over generations, till we reach a stopping criterion.

Genetic Algorithms are sufficiently randomized in nature, but they perform much better than random local search (in which we just try various random solutions, keeping track of the best so far), as they exploit historical information as well.

## Methodology

1. Initialization - The population size depends on the nature of the problem, but typically contains several hundreds or thousands of possible solutions. Often, the initial population is generated randomly, allowing the entire range of possible solutions (the search space). Occasionally, the solutions may be "seeded" in areas where optimal solutions are likely to be found.  
2. Selection - During each successive generation, a portion of the existing population is selected to breed a new generation. Individual solutions are selected through a fitness-based process, where fitter solutions (as measured by a fitness function) are typically more likely to be selected. Certain selection methods rate the fitness of each solution and preferentially select the best solutions. Other methods rate only a random sample of the population, as the former process may be very time-consuming. The fitness function is defined over the genetic representation and measures the quality of the represented solution. The fitness function is always problem dependent.
3. Crossover - In genetic algorithms and evolutionary computation, crossover, also called recombination, is a genetic operator used to combine the genetic information of two parents to generate new offspring. It is one way to stochastically generate new solutions from an existing population, and analogous to the crossover that happens during sexual reproduction in biology. Solutions can also be generated by cloning an existing solution, which is analogous to asexual reproduction. Newly generated solutions are typically mutated before being added to the population. Different variants of crossovers are - 

    * Single point crossover - A point on both parents' chromosomes is picked randomly, and designated a 'crossover point'. Bits to the right of that point are swapped between the two parent chromosomes. This results in two offspring, each carrying some genetic information from both parents.  
        ![](spc.png)
    * Two point and k-point crossover - In two-point crossover, two crossover points are picked randomly from the parent chromosomes. The bits in between the two points are swapped between the parent organisms.  
        ![](tpc.png)
    * Uniform crossover - In uniform crossover, typically, each bit is chosen from either parent with equal probability. Other mixing ratios are sometimes used, resulting in offspring which inherit more genetic information from one parent than the other. 
    
    * Crossovers for ordered lists - In some genetic algorithms, not all possible chromosomes represent valid solutions. In some cases, it is possible to use specialized crossover and mutation operators that are designed to avoid violating the constraints of the problem.

4. Mutation - Mutation is a genetic operator used to maintain genetic diversity from one generation of a population of genetic algorithm chromosomes to the next. It is analogous to biological mutation. Mutation alters one or more gene values in a chromosome from its initial state. In mutation, the solution may change entirely from the previous solution. Hence GA can come to a better solution by using mutation. Mutation occurs during evolution according to a user-definable mutation probability. This probability should be set low. If it is set too high, the search will turn into a primitive random search. Different variants of the mutation operator are - 
    * Bit string Mutation - The mutation of bit strings ensue through bit flips at random positions. The probability of a mutation of a bit is $\frac{1}{l}$ where l is the length of the binary vector. Thus, a mutation rate of 1 per mutation and individual selected for mutation is reached.  
    * Flip Bit - This mutation operator takes the chosen genome and inverts the bits (i.e. if the genome bit is 1, it is changed to 0 and vice versa). 
    * Boundary - This mutation operator replaces the genome with either lower or upper bound randomly. This can be used for integer and float genes. 
    * Non-uniform - The probability that amount of mutation will go to 0 with the next generation is increased by using non-uniform mutation operator. It keeps the population from stagnating in the early stages of the evolution. It tunes solution in later stages of evolution. This mutation operator can only be used for integer and float genes. 
    * Uniform - This operator replaces the value of the chosen gene with a uniform random value selected between the user-specified upper and lower bounds for that gene. This mutation operator can only be used for integer and float genes. 
    * Gaussian - This operator adds a unit Gaussian distributed random value to the chosen gene. If it falls outside of the user-specified lower or upper bounds for that gene, the new gene value is clipped. This mutation operator can only be used for integer and float genes. 
    * Shrink - This operator adds a random number taken from a Gaussian distribution with mean equal to the original value of each decision variable characterizing the entry parent vector. 

## Trace
The equation chosen for the optimisation is 
\begin{equation}f(x)=x^{3}-2 x^{2}+x, x ∈ [0, 31]\end{equation}

Population size is chosen to be 6 and the single point crossover and bit string mutation operators are used.

1. The initial population is generated randomly and the bit string is decoded into corresponding integers. The fitness of the chromosome is calculated by substituting into the function. The Range of number for use in Roulette selection is calculated by using the percentage contribution of each individual chromosome to the fitness of the generation. This ensures that chromosomes with high fitness value have a higher probability for getting selected.

|Sl no | Bit String | Integer value |  Fitness | Range (for use in Roulette selection)  |
|:----:|:----------:|:-------------:|:--------:|:--------------------------------------:|
|1     | 00101      | 5             | 80       | 0                                      | 
|2     | 11110      | 30            | 25230    | [1,56]                                 |
|3     | 01111      | 15            | 2940     | [57,63]                                |
|4     | 01011      | 11            | 1100     | [64,65]                                |
|5     | 10111      | 23            | 11132    | [66,90]                                |
|6     | 10001      | 17            | 4352     | [91,100]                               |

2. Roulette selection is used for selection of mating pairs. A random number from [1,100] is taken and the chromosome which contains the number in its range is chosen as one of the parents. Then a crossover point is chosen randomly from [1,4] to apply the crossover operator.

|  Sl no |  Parent 1|Parent 2|  Crossover Point| Offspring 1| Offspring 2|
|:------:|:--------:|:------:|:---------------:|:----------:|:----------:|
|1       | 11110    | 10111  | 4               | 11111      | 10110      |
|2       | 11110    | 10001  | 2               | 11001      | 10110      |
|3       | 11110    | 11110  | 1               | 11110      | 11110      |


3. We perform mutation on a couple of the offsprings

|  Offspring | Mutation Location|  New Offspring|
|:----------:|:----------------:|:-------------:|
|       11001|       5          |   11000       |
|       11110|       2          |   10110       |

4. The top 6 chromosomes according to the fitness value are passed as next generation

|Sl no | Bit String | Integer value |  Fitness |
|:----:|:----------:|:-------------:|:--------:|
|1     | 11111      | 31            | 27900    | 
|2     | 11110      | 30            | 25230    |
|3     | 11110      | 30            | 25230    |
|4     | 11000      | 24            | 12696    |
|5     | 10111      | 23            | 11132    |
|6     | 10110      | 22            | 9702     |

The above steps are repeated until the termination criteria is achieved. Here since one of the chromosomes has the highest achieveable value for the function, we terminate the algorithm. The maximum value of the function is found at x=31.

# 3. Bucket Brigade Classifier
The first major learning task facing any rule-based system operating in a complex environment is the credit assignment task, Somehow the performance system must determine both the rules responsible for its successes and the representativeness of the conditions encountered in attaining the successes. The bucket brigade algorithm is designed to solve the credit assignment problem for classifier systems. To implement the algorithm, each classifier is assigned a quantity called its strength. The bucket brigade algorithm adjusts the strength to reflect the classifier’s overall usefulness to the system. The strength is then used as the basis of a competition. Each time step, each satisfied classifier makes a bid based on its strength, and only the highest bidding classifiers get their messages on the message list for the next time step. It is worth recalling that there are no consistency requirements on posted messages; the message list can hold any set of messages, and any such set can direct further competition. The only point at which consistency enters is at the output interface. Here, different sets of messages may specify conflicting responses. Such conflicts are again resolved by competition. For example, the strengths of the classifiers advocating each response can be asummed so that one of the conflicting actions is chosen with a probability proportional to the sum of its advocates.

## Important Formula

* Bid made
\begin{equation}B_{i}=C_{b i d} S_{i}\end{equation}
    where $S_{i}$ is the strength of the classifier.
* $i^{th}$ classifier’s strength at time t+1 
\begin{equation}S_{i}(t+1)=S_{i}(t)-P_{i}(t)-T_{i}(t)+R_{i}(t)\end{equation}
    where 
        * S - Strength
        * P - Payment
        * T - Tax
        * R - Receipt
* Effective Bid 
\begin{equation}E B_{i}=B_{i}+N\left(\sigma_{\text {bid}}\right)\end{equation}
* Tax collected
\begin{equation}T_{i}=C_{\operatorname{tax}} S_{i}\end{equation}

## Trace

* t=0

|Sl | Classifier     | Strength | Messages | Match | Bid           |
|:-:|:--------------:|:--------:|:--------:|:-----:|:-------------:|
| 1 |    01##:0000   |     200  |          |   E   |  $200*0.1=20$ |
|  2|    00#0:1100   |     200  |          |       |               |
| 3 |     11##:1000  |    200   |          |       |               |
| 4 |      ##00:0001 |    200   |          |       |               |
|   |     Environment|   0      |    0111  |       |               |

* t=1

|Sl | Classifier     | Strength| Messages| Match| Bid            |
|:-:|:--------------:|:-------:|:-------:|:----:|:--------------:|
| 1 |    01##:0000   |     180 |    0000 |      |                |
|  2|    00#0:1100   |     200 |         |   1  |    $200*0.1=20$|
| 3 |     11##:1000  |    200  |         |      |                |
| 4 |      ##00:0001 |    200  |         |    1 |    $200*0.1=20$|
|   |     Environment|   20    |         |      |                |

* t=2

|Sl | Classifier     | Strength| Messages| Match| Bid         |
|:-:|:--------------:|:-------:|:-------:|:----:|:-----------:|
| 1 |    01##:0000   |     220 |         |      |             |
|  2|    00#0:1100   |     180 |   1100  |      |             |
| 3 |     11##:1000  |    200  |         |  2   | $200*0.1=20$|
| 4 |      ##00:0001 |    180  |   0001  |  2   | $180*0.1=18$|
|   |     Environment|   20    |         |      |             |

* t=3

|Sl | Classifier     | Strength| Messages| Match| Bid         |
|:-:|:--------------:|:-------:|:-------:|:----:|:-----------:|
| 1 |    01##:0000   |     200 |         |      |             |
|  2|    00#0:1100   |     218 |         |      |             |
| 3 |     11##:1000  |    180  |   1000  |      |             |
| 4 |      ##00:0001 |    162  |    0001 | 3    | $162*0.1=16$|
|   |     Environment|   20    |         |      |             |

* t=4

|Sl | Classifier     | Strength| Messages| Match| Bid|
|:-:|:--------------:|:-------:|:-------:|:----:|:--:|
| 1 |    01##:0000   |     220 |         |      |    |
|  2|    00#0:1100   |     218 |         |      |    |
| 3 |     11##:1000  |    196  |         |      |    |
| 4 |      ##00:0001 |     146 |    0001 |      |    |
|   |     Environment|   20    |         |      |    |

* t=5, A reward of 50 is awarded to the last active classifier.

|Sl | Classifier     | Strength| Messages| Match| Bid|
|:-:|:--------------:|:-------:|:-------:|:----:|:--:|
| 1 |    01##:0000   |     220 |         |      |    |
|  2|    00#0:1100   |     218 |         |      |    |
| 3 |     11##:1000  |    196  |         |      |    |
| 4 |      ##00:0001 |     196 |         |      |    |
|   |     Environment|   20    |         |      |    |

# 5. Metrics to evaluate models
## Associated terms

* **True positives (TP)**: These refer to the positive tuples that were correctly labeled by
the classifier. Let TP be the number of true positives.
* **True negatives (TN )**: These are the negative tuples that were correctly labeled by the
classifier. Let TN be the number of true negatives.
* **False positives (FP)**: These are the negative tuples that were incorrectly labeled as
positive (e.g., tuples of class buys computer = no for which the classifier predicted
buys computer = yes). Let FP be the number of false positives.
* **False negatives (FN )**: These are the positive tuples that were mislabeled as neg-
ative (e.g., tuples of class buys computer = yes for which the classifier predicted
buys computer = no). Let FN be the number of false negatives.

## Metrics

* Accuracy(Recognition Rate) - 
    \begin{equation} Accuracy = \frac{TP+TN}{P+N} \end{equation}
* Error Rate(Misclassification Rate) - 
    \begin{equation} Error Rate = \frac{FP+FN}{P+N} \end{equation}
* Sensitivity(Recall) - 
    \begin{equation} Recall = \frac{TP}{P} \end{equation}
* Specificity(True Negative Rate) - 
    \begin{equation} Specificity = \frac{TN}{N} \end{equation}
* Precision - 
    \begin{equation} Precision = \frac{TP}{TP+FP} \end{equation}
* F,F\textsubscript{1},F score - 
    \begin{equation} F = \frac{2*precision*recall}{precision+recall} \end{equation}
* F\textsubscript{$\beta$} -  
    where $\beta$ is a non-negative real number
    \begin{equation}F_{\beta} = \frac{\left(1+\beta^{2}\right) \times \text { precision } \times \text { recall }}{\beta^{2} \times \text { precision }+\text { recall }}\end{equation}

## Confusion Matrix
|                       |   CLASS 1 Predicted         |CLASS2 Predicted            |    |
|:---------------------:|:---------------------------:|:--------------------------:|:--:|
|  CLASS 1 Actual       |      TP                     | FN                         |  P |
|  CLASS 2 Actual       |       FP                    |   TN                       |  N |
|---------------------- |-----------------------------|----------------------------|----|
|         Total         |           P'                |         N'                 | P+N|

## Examples
**Statistics used**

|                       |   CLASS 1 Predicted         |CLASS2 Predicted            |        |
|:---------------------:|:---------------------------:|:--------------------------:|:------:|
|  CLASS 1 Actual       |      6954                   | 46                         |  7000  |
|  CLASS 2 Actual       |      412                    |   2588                     |  3000  |
|---------------------- |-----------------------------|----------------------------|--------|
|        Total          |          7366               |         2634               | 10000  |

- Accuracy = $\frac{6954+2588}{10000}=0.7$
- Error Rate = $\frac{412+46}{10000}=0.0458$
- Recall = $\frac{6954}{7000}=0.9934$
- Specificity = $\frac{2588}{3000}=0.8627$
- Precision = $\frac{6954}{7366}=0.9441$
- F score = $\frac{2*0.9441*0.9934}{0.9441+0.9934}=0.9681$
- F\textsubscript{0.3}=$\frac{\left(1+0.3^{2}\right) \times \text { precision } \times \text { recall }}{0.3^{2} \times \text { precision }+\text { recall }}=0.0012$

# 6. Clustering

**Dataset Used **

|  Individual| x   |
|------------|-----|
|       1    | 1.0 |
|        2   | 2.0 |
|        3   | 1.5 |
|       4    |  3.5|
|       5    | 2.0 |
|       6    | 4.0 |
|       7    | 5.0 |
|       8    | 6.0 |
|       9    | 5.5 |
|       10   | 4.5 |

## a) k-means clustering

### Pseudocode

```
Step 1: Begin with a decision on the value of k = number of clusters .

Step 2: Put any initial partition that classifies the data into k  clusters. You may  assign the training samples randomly,or systematically as the following: 
       1.Take the first k training sample as single-element clusters      
       2. Assign each of the remaining (N-k) training sample to the cluster with the nearest centroid. After each assignment, recompute the centroid of the gaining  cluster. 

Step 3: Take each sample in sequence and compute its distance from the centroid of each of the clusters. If a sample is not currently in the cluster with the closest centroid, switch this sample to that cluster and update the centroid of the cluster gaining the new sample and the cluster losing the sample. 

Step 4 . Repeat step 3 until convergence is achieved, that is until a pass through the training sample causes no new assignments. 

```

### Trace

* K = 2
* Manhattan distance is used.

**Iteration 1**

* Random centroids are chosen - 
    * C\textsubscript{1} = 1.0
    * C\textsubscript{2} = 4.0

|  Individual | x   |Distance from C\textsubscript{1}|Distance from C\textsubscript{2}|Cluster| 
|:-----------:|:---:|:-------------------------:|:-------------------------:|:-----:|
|       1     | 1.0 |              0            |             3             |   1   |
|        2    | 2.0 |              1            |             2             |   1   |
|        3    | 1.5 |              0.5          |              2.5          |   1   |
|       4     |  3.5|               2.5         |               0.5         |   2   |
|       5     | 2.0 |               1           |              2            |   1   |
|       6     | 4.0 |               3           |              0            |    2  |
|       7     | 5.0 |               4           |              1            |    2  |
|       8     | 6.0 |               5           |               2           |    2  |
|       9     | 5.5 |               4.5         |               1.5         |    2  |
|       10    | 4.5 |               3.5         |               0.5         |    2  |

**Iteration 2**

* New centroids are - 
    * C\textsubscript{1} = $\frac{1+2+1.5+2}{4}=1.625$
    * C\textsubscript{2} = $\frac{3.5+4.0+5.0+6.0+5.5+4.5}{6}=4$

|  Individual | x   |Distance from C\textsubscript{1}|Distance from C\textsubscript{2}|Cluster| 
|:-----------:|:---:|:-------------------------:|:-------------------------:|:-----:|
|       1     | 1.0 |              0.625        |             3             |   1   |
|        2    | 2.0 |              0.375        |             2             |   1   |
|        3    | 1.5 |              0.125        |              2.5          |   1   |
|       4     |  3.5|               1.875       |               0.5         |   2   |
|       5     | 2.0 |               0.375       |              2            |   1   |
|       6     | 4.0 |               2.375       |              0            |    2  |
|       7     | 5.0 |               3.375       |              1.5          |    2  |
|       8     | 6.0 |               4.375       |               2           |    2  |
|       9     | 5.5 |               3.875       |               3.5         |    2  |
|       10    | 4.5 |               2.875       |               0.5         |    1  |

* No change in cluster values. Hence stop iteration.

## b) k-medoids clustering

In medoids clustering the metric which is used to evaulate is called the dissimilarity measure given by - 
\begin{equation}c=\sum_{C i} \sum_{P i \in C i}|P i-C i|\end{equation}

### Pseudocode
```
1. Initialize: select k random points out of the n data points as the medoids.

2. Associate each data point to the closest medoid by using any common distance metric methods.

3. While the cost decreases:
        For each medoid m, for each data o point which is not a medoid:
                1. Swap m and o, associate each data point to the closest medoid, recompute the cost.
                2. If the total cost is more than that in the previous step, undo the swap. 

```

### Trace

We use the same dataset used in the previous trace.

* We choose x=1 and x=4.5 as menoids and manhattan distance - 

| x   |Distance from x=1          |Distance from x=4          | 
|:---:|:-------------------------:|:-------------------------:|
| 1.0 |              0            |             3.5           |
| 2.0 |              1            |             2.5           |
| 1.5 |              0.5          |              2.5          |
|  3.5|               2.5         |               1           |
| 2.0 |               1           |              2.5          |
| 4.0 |               3           |              0.5          |
| 5.0 |               4           |              0.5          |
| 6.0 |               5           |               1.5         |
| 5.5 |               4.5         |               1           |
| 4.5 |               3.5         |               0           |

* Clusters are - {1,2,1.5,2}, {4.5,3.5,4,5,6,5.5}
* Total cost = $1+0.5+1+1+0.5+1+1.5+15+1=7.5$
* Now we consider x = 4 instead of x=4.5.

| x   |Distance from x=1          |Distance from x=4          | 
|:---:|:-------------------------:|:-------------------------:|
| 1.0 |              0            |             3             |
| 2.0 |              1            |             2             |
| 1.5 |              0.5          |              2.5          |
|  3.5|               2.5         |               0.5         |
| 2.0 |               1           |              2            |
| 4.0 |               3           |              0            |
| 5.0 |               4           |              1            |
| 6.0 |               5           |               2           |
| 5.5 |               4.5         |               1.5         |
| 4.5 |               3.5         |               0.5         |

* Clusters are - {1,2,1.5,2}, {4,3.5,5,6,5.5,4.5}
* Total cost = $1+0.5+1+0.5+1+2+1.5+0.5=8$
* Since new cost is larger than previous cost, we choose x=4.5 over x=4 and since the clusters dont change, we terminate the algorithm.
* Clusters are - {1,2,1.5,2}, {4.5,3.5,4,5,6,5.5}

## c) Hierarchial Clustering

* A typical clustering analysis approach via partitioning data set sequentially
* Construct nested partitions layer by layer via grouping objects into a tree of clusters
(without the need to know the number of clusters in advance)
* Use (generalised) distance matrix as clustering criteria

**Agglomerative vs. Divisive**  

* Two sequential clustering strategies for constructing a tree of clusters
* Agglomerative: a bottom-up strategy
    * Initially each data object is in its own (atomic) cluster
    * Then merge these atomic clusters into larger and larger clusters
* Divisive: a top-down strategy
    * Initially all objects are in one single cluster
    * Then the cluster is subdivided into smaller and smaller clusters

**Cluster Distance Measures **

* **Single Link** - smallest distance between an element in one cluster and an element in the other.
\begin{equation}\mathrm{d}\left(\mathrm{C}_{\mathrm{i}}, \mathrm{C}_{\mathrm{j}}\right)=\operatorname{min}\left\{\mathrm{d}\left(\mathrm{x}_{\mathrm{ip}}, \mathrm{x}_{\mathrm{jq}}\right)\right\}\end{equation}

* **Complete Link ** - largest distance between an element in one cluster and an element in the other.
\begin{equation}\mathrm{d}\left(\mathrm{C}_{\mathrm{i}}, \mathrm{C}_{\mathrm{j}}\right)=\operatorname{max}\left\{\mathrm{d}\left(\mathrm{x}_{\mathrm{ip}}, \mathrm{x}_{\mathrm{jq}}\right)\right\}\end{equation}

* **Average Link ** - avg distance between average elements in one cluster and elements in the other.
\begin{equation}\mathrm{d}\left(\mathrm{C}_{\mathrm{i}}, \mathrm{C}_{\mathrm{j}}\right)=\operatorname{avg}\left\{\mathrm{d}\left(\mathrm{x}_{\mathrm{ip}}, \mathrm{x}_{\mathrm{jq}}\right)\right\}\end{equation}

### Aglomerative clustering  

**Pseudocode**  

```
1. Assign each object to a separate cluster.
2. Evaluate all pair-wise distances between clusters (distance metrics are described in Distance Metrics Overview).
3. Construct a distance matrix using the distance values.
4. Look for the pair of clusters with the shortest distance.
5. Remove the pair from the matrix and merge them.
6. Evaluate all distances from this new cluster to all other clusters, and update the matrix.
7. Repeat until the distance matrix is reduced to a single element.

```

**Trace**
We use Single link for inter cluster distance measure.

* Initial distance matrix

|      |   1  |   2   |   3   |   4  |   5  |  6   |   7   |   8  |   9  |  10   |
|:----:|:----:|:-----:|:-----:|:----:|:----:|:----:|:-----:|:----:|:----:|:-----:|
|   1  |   0  |   1   |  0.5  |  2.5 |    1 |    3 |   4   |    5 |   4.5|   3.5 |
|   2  |   1  |   0   |   0.5 |   1.5|   0  |   2  |   3   |    4 |  3.5 |   2.5 |
|   3  |   0.5|   0.5 |  0    |    2 |   0.5|   2.5|   3.5 |   4.5|   4  |   3   |
|   4  |   2.5|   1.5 |   2   | 0    |   1.5|  0.5 |    1.5|   2.5|   1  |    1  |
|   5  |   1  |   0   |   0.5 |   1.5|  0   | 2    |   1   |   2  |  1.5 |   0.5 |
|   6  |  3   |   2   |    2.5|   0.5|    2 | 0    |  1    |   1  |  0.5 |    0.5|
|   7  |  4   |   3   |   3.5 |   1.5|   1  |  1   | 0     |  1   |  0.5 |    1.5|
|   8  |  5   |   4   |    4.5|   2.5|   2  |  1   | 1     | 0    |  0.5 |   1.5 |
|   9  |  4.5 |    3.5|      4|   2  |  1.5 |  0.5 |  0.5  |   0.5|  0   |   1   |
|   10 |   3.5|   2.5 |    3  |   1  |   0.5|  0.5 |   1.5 |   1.5|   1  |  0    |

* The first pair is (2,5)

|      |   1  | (2,5) |   3   |   4  |  6   |   7   |   8  |   9  |  10   |
|:----:|:----:|:-----:|:-----:|:----:|:----:|:-----:|:----:|:----:|:-----:|
|   1  |   0  |   1   |  0.5  |  2.5 |    3 |   4   |    5 |   4.5|   3.5 |
|(2,5) |   1  |   0   |   0.5 |   1.5|   2  |   3   |    4 |  3.5 |   2.5 |
|   3  |   0.5|   0.5 |  0    |    2 |   2.5|   3.5 |   4.5|   4  |   3   |
|   4  |   2.5|   1.5 |   2   | 0    |  0.5 |    1.5|   2.5|   1  |    1  |
|   6  |  3   |   2   |    2.5|   0.5| 0    |  1    |   1  |  0.5 |    0.5|
|   7  |  4   |   3   |   3.5 |   1.5|  1   | 0     |  1   |  0.5 |    1.5|
|   8  |  5   |   4   |    4.5|   2.5|  1   | 1     | 0    |  0.5 |   1.5 |
|   9  |  4.5 |    3.5|      4|   2  |  0.5 |  0.5  |   0.5|  0   |   1   |
|   10 |   3.5|   2.5 |    3  |   1  |  0.5 |   1.5 |   1.5|   1  |  0    |

* The next pair is (1,3)

|      | (1,3)| (2,5) |   4  |  6   |   7   |   8  |   9  |  10   |
|:----:|:----:|:-----:|:----:|:----:|:-----:|:----:|:----:|:-----:|
|(1,3) |   0  |   0.5 |  2   |  2.5 |   3.5 | 4.5  |   4  |   3   |
|(2,5) |   0.5|   0   |   1.5|   2  |   3   |    4 |  3.5 |   2.5 |
|   4  |   2  |   1.5 | 0    |  0.5 |    1.5|   2.5|   1  |    1  |
|   6  |  2.5 |   2   |   0.5| 0    |  1    |   1  |  0.5 |    0.5|
|   7  |  3.5 |   3   |   1.5|  1   | 0     |  1   |  0.5 |    1.5|
|   8  |  4.5 |   4   |   2.5|  1   | 1     | 0    |  0.5 |   1.5 |
|   9  |  4   |    3.5|   2  |  0.5 |  0.5  |   0.5|  0   |   1   |
|   10 |   3  |   2.5 |   1  |  0.5 |   1.5 |   1.5|   1  |  0    |

* The next pair is (4,6)

|      | (1,3)| (2,5) | (4,6)|   7   |   8  |   9  |  10   |
|:----:|:----:|:-----:|:----:|:-----:|:----:|:----:|:-----:|
|(1,3) |   0  |   0.5 |  2   |   3.5 | 4.5  |   4  |   3   |
|(2,5) |   0.5|   0   |   1.5|   3   |    4 |  3.5 |   2.5 |
|(4,6) |   2  |   1.5 | 0    |    1  |   1  |   0.5|    0.5|
|   7  |  3.5 |   3   |   1  | 0     |  1   |  0.5 |    1.5|
|   8  |  4.5 |   4   |   1  | 1     | 0    |  0.5 |   1.5 |
|   9  |  4   |    3.5|   0.5|  0.5  |   0.5|  0   |   1   |
|   10 |   3  |   2.5 |   0.5|   1.5 |   1.5|   1  |  0    |

* The next pair is X where X = ((1,3),(2,5))

|      | X    | (4,6)|   7   |   8  |   9  |  10   |
|:----:|:----:|:----:|:-----:|:----:|:----:|:-----:|
|X     |   0  |  1.5 |   3   | 4    |   3.5|   2.5 |
|(4,6) |   1.5| 0    |    1  |   1  |   0.5|    0.5|
|   7  |  3   |   1  | 0     |  1   |  0.5 |    1.5|
|   8  |  4   |   1  | 1     | 0    |  0.5 |   1.5 |
|   9  |  3.5 |   0.5|  0.5  |   0.5|  0   |   1   |
|   10 |   2.5|   0.5|   1.5 |   1.5|   1  |  0    |


* The next pair is (A,10) where A - (4,6)

|      | X    |(A,10)|   7   |   8  |   9  |
|:----:|:----:|:----:|:-----:|:----:|:----:|
|X     |   0  |  1.5 |   3   | 4    |   3.5|
|(A,10)|   1.5| 0    |    1  |   1  |   0.5|
|   7  |  3   |   1  | 0     |  1   |  0.5 |
|   8  |  4   |   1  | 1     | 0    |  0.5 |
|   9  |  3.5 |   0.5|  0.5  |   0.5|  0   |

* The next pair is (B,7) where B - (A,10)

|      | (1,3)|(B,7) |   8  |   9  |
|:----:|:----:|:----:|:----:|:----:|
|X     |   0  |  1.5 | 4    |   3.5|
|(B,7) |   1.5| 0    |   1  |   0.5|
|   8  |  4   |   1  | 0    |  0.5 |
|   9  |  3.5 |   0.5|   0.5|  0   |

* The next pair is (C,9) where C - (B,7)

|      | (1,3)|(C,9) |   8  |
|:----:|:----:|:----:|:----:|
|X     |   0  |  1.5 | 4    |
|(C,9) |   1.5| 0    |   0.5|
|   8  |  4   |   0.5| 0    |

* The next pair is (D,8) where D - (C,9)

|      | (1,3)|(D,8) |
|:----:|:----:|:----:|
|X     |   0  |  1.5 |
|(D,8) |   1.5| 0    |

* The next pair is (X,Y) where Y - (D,8)


### Divisive Clustering
**Pseudocode**

```
1. Given a dataset of size N.
2. At the top we have all data in one cluster.
3. The cluster is split using a flat clustering method (eg: K-Means).
4. For each cluster go back to step 2 and repeat till each data is in its own singleton cluster.
```

**Trace**

* C = {1,2,3,4,5,6,7,8,9,10}
* On splitting C
    * C\textsubscript{1} = {1,2,3,5}
    * C\textsubscript{2} = {4,6,7,8,9,10}
* On splitting C\textsubscript{1}
    * C\textsubscript{11} = {1,3}
    * C\textsubscript{12} = {2,5}
* On splitting C\textsubscript{11} we get individual elements 1 and 3.
* On splitting C\textsubscript{12} we get individual elements 2 and 5.
* On splitting C\textsubscript{2}
    * C\textsubscript{21} = {4,6,7,9,10}
    * Individual element 8.
* On splitting C\textsubscript{21}
    * C\textsubscript{22} = {4,6,7,10}
    * Individual element 9.
* On splitting C\textsubscript{22}
    * C\textsubscript{23} = {4,6,10}
    * Individual element 7.
* On splitting C\textsubscript{23}
    * C\textsubscript{24} = {4,6}
    * Individual element 10.
* On splitting C\textsubscript{24} we get individual elements 4 and 6.

The dendogram for both the methods look similar.

![Dendogram for hierarchial clustering](agglomerative.png)

# 7. Various Distance Measures

* Eucledian Distance - 
    \begin{equation}d(x, y)=\sqrt[2]{\sum_{i=1}^{p}\left|x_{i}-y_{i}\right|^{2}}\end{equation}
* Manhattan Distance - 
    \begin{equation}d(x, y)=\sum_{i=1}^{p}\left|x_{i}-y_{i}\right|\end{equation}
* Maximum Norm - 
    \begin{equation}d(x, y)=\max _{ 1 \leq i \leq p}\left|x_{i}-y_{i}\right|\end{equation}
* Pearson Correlation Distance - 
    \begin{equation}d(x, y)=1-\frac{\sum_{i=1}^{n}\left(x_{i}-\bar{x}\right)\left(y_{i}-\bar{y}\right)}{\sqrt{\sum_{i=1}^{n}\left(x_{i}-\bar{x}\right)^{2} \sum_{i=1}^{n}\left(y_{i}-\bar{y}\right)^{2}}}\end{equation}
    where $\bar{x}=\frac{1}{p} \sum_{i=1}^{p} x_{i}$ and $\bar{y}=\frac{1}{p} \sum_{i=1}^{p} y_{i}$
* Eisen Cosine Correlation Distance - 
    \begin{equation}d(x, y)=1-\frac{\left|\sum_{i=1}^{n} x_{i} y_{i}\right|}{\sqrt{\sum_{i=1}^{n} x_{i}^{2} \sum_{i=1}^{n} y_{i}^{2}}}\end{equation}

## Example

X\textsubscript{1}=(1,2)  
X\textsubscript{2}=(5,6)

* Eucledian Distance=$\sqrt[2]{\left|1-5\right|^{2}+\left|2-6\right|^{2}}=4\sqrt[2]{2}=5.6568$
* Manhattan Distance=$\left|1-5\right|+\left|2-6\right|=8$
* Maximum Norm=$\max{\left|1-5\right|,\left|2-6\right|}=4$
* Pearson Correlation Distance=$1-1=0$
* Eisen Cosine Correlation Distance=$1-\frac{1*5+2*6}{\sqrt[2]{1*4+25*36}}=0.4346$

# 8. Python Implementations


```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
```

## Decision Tree


```python
data = datasets.load_iris()
dataset = pd.DataFrame(data.data, columns=data.feature_names)
dataset.shape
```

> (150, 4)



```python
dataset.head()
```

> |      | sepal length(cm) | sepal width(cm) |petal length(cm) | petal width(cm)|
> |:----:|:----------------:|:---------------:|:---------------:|:--------------:|
> |   0  |  5.1             |  3.5            | 1.4             |   0.2          |
> |  1   |  4.9             |      3.0        |   1.4           |  0.2           |
> |  2   |  4.7             |      3.2        |     1.3         |     0.2        |
> |   3  |    4.6           |        3.1      |   1.5           |       0.2      |
> |   4  |      5.0         |          3.6    |   1.4           |         0.2    |


```python
X = dataset
y = pd.DataFrame(data.target,columns=['Class'])
print(X.shape)
print(y.shape)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)
```


> (150, 4)
> (150, 1)



```python
classifier = DecisionTreeClassifier()
classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))
```

> ```
>     [[ 9  0  0]
>      [ 0 10  1]
>      [ 0  1  9]]
>                   precision    recall  f1-score   support
>     
>                0       1.00      1.00      1.00         9
>                1       0.91      0.91      0.91        11
>                2       0.90      0.90      0.90        10
>     
>         accuracy                           0.93        30
>        macro avg       0.94      0.94      0.94        30
>     weighted avg       0.93      0.93      0.93        30
>     
> ```

## Naive Bayes


```python
nv = GaussianNB()
nv.fit(X_train,y_train['Class'].values.tolist())
y_pred = nv.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))
```
> ```
>     [[ 9  0  0]
>      [ 0  9  2]
>      [ 0  0 10]]
>                   precision    recall  f1-score   support
>     
>                0       1.00      1.00      1.00         9
>                1       1.00      0.82      0.90        11
>                2       0.83      1.00      0.91        10
>     
>         accuracy                           0.93        30
>        macro avg       0.94      0.94      0.94        30
>     weighted avg       0.94      0.93      0.93        30
> ``` 


## Neural Network(Back propagation)


```python
mlp = MLPClassifier(max_iter=10000)
mlp.fit(X_train,y_train['Class'].values.tolist())
y_pred = mlp.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))
```

> ```
>     [[ 9  0  0]
>      [ 0 11  0]
>      [ 0  0 10]]
>                   precision    recall  f1-score   support
>     
>                0       1.00      1.00      1.00         9
>                1       1.00      1.00      1.00        11
>                2       1.00      1.00      1.00        10
>     
>         accuracy                           1.00        30
>        macro avg       1.00      1.00      1.00        30
>     weighted avg       1.00      1.00      1.00        30
>     
> ```

## K-means Clustering


```python
X, y = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)
kmeans = KMeans(n_clusters=4, init='k-means++', max_iter=300, n_init=10, random_state=0)
pred_y = kmeans.fit_predict(X)
plt.scatter(X[:,0], X[:,1])
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=300, c='red')
plt.show()
```


> ![png](output_12_0.png)


## Hierarchial Clustering


```python
cluster = AgglomerativeClustering(n_clusters=4, affinity='euclidean', linkage='ward')
cluster.fit_predict(X)
plt.scatter(X[:,0],X[:,1], c=cluster.labels_, cmap='rainbow')
plt.show()
```


> ![png](output_14_0.png)

